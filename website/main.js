$(document).ready(function(){
			/*if( /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent) ) {
				window.location = "mobile.html";
			}*/
			
			$('#page').fadeOut(0);
			
            $(".navs").hide();
            $("#close_button_art").hide();
						
			$(".en").hide();
						
			$('#page').fadeIn(1200);
});
$(document).keydown(function(e){
	if ($('#page1').css('left') == '0px' || $('#page2').css('left') == '0px' || $('#page3').css('left') == '0px') {
		if (e.keyCode == 37) {
		   prev_page();
		   lefted = true;	
		   return false;
		}
		else if (e.keyCode == 39) {
		   next_page();
		   return false;
		}
	}
	if (e.keyCode == 27)
	{
		close_article();
		close_nav();
	}
});

 function toggle_article(arg){
                for (var i=1; i <= 12; i++)
                {
                  if (i != arg)
                    $("#article"+i).slideUp(650);
                }
                $("#article"+arg).slideToggle(350, function() {
				
				
				if ($("#article"+arg).css('display') == "none")
				{
					$("#close_button_art").fadeOut(450);
					$('html, body').animate({
						scrollTop:$("#top	").offset().top
					}, 'slow');
				}
				else
				{
					$("#close_button_art").fadeIn(450);
					$('html, body').animate({
						scrollTop:$("#reader").offset().top
					}, 'slow');
				}
				});
};
 function close_article(){
                for (var i=1; i < 9; i++)
                {
                    $("#article"+i).slideUp(350);
                }

                $("#close_button_art").fadeToggle();

                $('html, body').animate({
                    scrollTop:$("#top").offset().top
                }, 'slow');
          };


 function toggle_nav(arg){
   if (arg == 1) {
        $("#nav2").slideUp();
        $("#nav3").slideUp();
    }
    else if (arg == 2) {
        $("#nav1").slideUp();
        $("#nav3").slideUp();
    }
	else
	{
        $("#nav1").slideUp();
        $("#nav2").slideUp();
	}

    $("#nav"+arg).slideToggle();
    $("#close_button_nav").fadeIn();

    $('html, body').animate({
        scrollTop:$("#top").offset().top
    }, 'slow');

};
 function close_nav(){
                $("#nav3").slideUp();
                $("#nav2").slideUp();
                $("#nav1").slideUp();
                $("#close_button_nav").fadeToggle();

                $('html, body').animate({
                    scrollTop:$("#top").offset().top
                }, 'slow');
          };

 function prev_page() {
	if ($('#page1').css('left') == '0px' || $('#page2').css('left') == '0px' || $('#page3').css('left') == '0px') {
		for (var i=1; i <= 3; i++)
		{
			if ($('#page' + i).css('left') == '1888px')
			   {
					$('#page' + i).css('left',	'-944px');
			   }  
		}
		check_page("-");

		$('#page1').animate({
						left: '+=944'
					}, 'slow');
					
		$('#page2').animate({
						left: '+=944'
					}, 'slow');
		$('#page3').animate({
						left: '+=944'
					}, 'slow');
	}
 };
 function next_page() {		
	if ($('#page1').css('left') == '0px' || $('#page2').css('left') == '0px' || $('#page3').css('left') == '0px') {
		for (var i=1; i <= 3; i++)
		{
			if ($('#page' + i).css('left') == '-1888px')
			   {
					$('#page' + i).css('left',	'944px');			
			   }  
		}	
		check_page("");
	   
	   $('#page1').animate({
						left: '-=944'
					}, 'slow');

	   $('#page2').animate({
						left: '-=944'
					}, 'slow');

	   $('#page3').animate({
						left: '-=944'
					}, 'slow');
	}
};
 function check_page(arg)
 {
	for (var i=1; i <= 3; i++)
	{
		if ($('#page' + i).css('left') == arg + "944px")
				$('#dot' + i).css('opacity','1');
		else
				$('#dot' + i).css('opacity','0.35');
	}
}
function up_page()
{
	if ($('#presentation').css('top') == '0px' || $('#presentation').css('top') == '475px') 
	{
	
		if ($('#presentation').css('top') > '0px') {
			$('#presentation').css('top','-475px');			
		} 
		if ($('#news').css('top') > '-274px') {
			$('#news').css('top','-548px');	
		}
		$('#news').animate({
						top: '+=274'
					}, 'slow');
		$('#presentation').animate({
						top: '+=475'
					}, 'slow');
	}
}

function lang_switch(lang) {
	if (lang == 'fr')
	{
		$(".en").fadeOut(100, function() {
		$(".fr").fadeIn(100);
		});
	}
	else
	{
		$(".fr").fadeOut(100, function() {
		$(".en").fadeIn(100);
		});
	}
}