/*
* Elias coding
* Project: V.O.X.
* Developer: TheSandmen
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <err.h>

int curr_bit = 0;
unsigned char buffer = 0;

void WriteBit(int bit, FILE* file)
{
    buffer <<= 1;
    if (bit)
    buffer |= 0x1;

    if (++curr_bit == 8)
    {
        fwrite(&buffer, 1, 1, file);
        curr_bit = 0;
        buffer = 0;
    }
}

void FlushBits(FILE* file)
{
    while (curr_bit)
    WriteBit(0, file);
}

void eliasEncode(char* input_name, char* output_name)
{
    int k = 0;
    struct stat istics;

    printf("%d\n", k++);

    if (stat(input_name, &istics))
    err(EXIT_FAILURE, "Could not retrieve informations on file.");

    printf("%d\n", k++);
    FILE *input_file = fopen(input_name, "rb");
    if (!input_file)
    errx(EXIT_FAILURE, "Cannot open %s.", input_name);

    printf("%d\n", k++);
    FILE *output_file = fopen(output_name, "wb");
    if (!output_file)
    errx(EXIT_FAILURE, "Cannot open %s.", output_name);

    printf("%d\n", k++);
    char* c = NULL;
    c = malloc(istics.st_size);
    printf("%d\n", k++);

    if (fread(c, 1, istics.st_size, input_file) < (unsigned long)istics.st_size)
    err(EXIT_FAILURE, "Could not read file.");

    printf("%d\n", k++);
    for (size_t i = 0; i < (unsigned long)(istics.st_size / 8); i++)
    {
        printf("%d\n", k++);
        size_t in = (int)c[i];
        int len = 0;
        int length = 0;

        for (size_t temp = in; temp > 0; temp >>= 1)
        len++;

        printf("%d\n", k++);
        for (int temp = len; temp > 1; temp >>= 1)
        length++;

        printf("%d\n", k++);
        for (int i = length; i > 0; i--)
        WriteBit(0, output_file);

        printf("%d\n", k++);
        for (int i = length; i >= 0; i--)
        WriteBit((len >> 1) & 1, output_file);

        printf("%d\n", k++);
        for (int i = len - 2; i >= 0; i--)
        WriteBit((in >> i) & 1, output_file);
    }

    printf("%d\n", k++);
    FlushBits(output_file);
    fclose(input_file);
    fclose(output_file);
    free(c);

    return;
}

int getNextBit(char* c)
{
    static size_t offset, count;
    if (&offset == NULL)
    {
        offset = 0;
        count = 0;
    }

    if (offset >= 8)
    {
        offset = 0;
        count++;
        printf("NEW CHAR ----------------------------------\n");
    }

    return ((int)c[count] >> offset++) & 0x01;
}

void eliasDecode(char* input_name, char* output_name)
{
    int k = 0;
    struct stat istics;

    printf("%d\n", k++);

    if (stat(input_name, &istics))
    err(EXIT_FAILURE, "Could not retrieve informations on file.");

    printf("%d\n", k++);
    FILE *input_file = fopen(input_name, "rb");
    if (!input_file)
    errx(EXIT_FAILURE, "Cannot open %s.", input_name);

    printf("%d\n", k++);
    FILE *output_file = fopen(output_name, "wb");
    if (!output_file)
    errx(EXIT_FAILURE, "Cannot open %s.", output_name);

    printf("%d\n", k++);
    char* c = NULL;
    c = malloc(istics.st_size);
    printf("%d\n", k++);

    if (fread(c, 1, istics.st_size, input_file) < (unsigned long)istics.st_size)
    err(EXIT_FAILURE, "Could not read file.");

    printf("%d\n", k++);
    for (size_t i = 0; i < (unsigned long)(istics.st_size); i++)
    {
        printf("%d\n", k++);
        size_t in = 1;
        int len = 1;
        int length = 0;

        while (!getNextBit(c))
        length++;

        printf("%d\n", k++);

        for (int i = 0; i < length; i++)
        {
            len <<= 1;

            if (getNextBit(c))
            len |= 1;
        }

        printf("%d\n", k++);
        for (int i = 0; i < len-1; i++)
        {
            in <<= 1;

            if (getNextBit(c))
            in |= 1;
        }

        printf("%d\n", k++);
        char out = (char)in;
        fwrite(&out, 1, 1, output_file);
    }

    printf("%d\n", k++);
    fclose(input_file);
    fclose(output_file);

    return;
}



int main(int argc, char* argv[])
{
    if (argc < 4)
    {
        printf("Missing argument.\n");
        return 1;
    }

    if (!strcmp("-e", argv[1]))
    eliasEncode(argv[2], argv[3]);
    else if (!strcmp("-d", argv[1]))
    eliasDecode(argv[2], argv[3]);

    return 0;
}
