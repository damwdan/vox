(*
 * Graphical User Interface
 * Project: V.O.X.
 * Developer: TheSandmen
 *)

(* ---------- main ---------- *)

open Unix;;

let _ = GMain.init ()

let window = GWindow.window ~width:800 ~height:600 ~resizable:true ~position:`CENTER
  ~title:"V.O.X." ()

let mainbox = GPack.vbox ~spacing:10 ~border_width:10 ~packing:window#add ()

let logoBox = GPack.vbox ~packing:mainbox#pack ()

let logo = GMisc.image ~packing:logoBox#add ()

let _ =
  let logoPixbuf = GdkPixbuf.from_file_at_size "logo.png" 258 60 in
    logo#set_pixbuf logoPixbuf

let toolbar =
  GButton.toolbar ~orientation:`HORIZONTAL ~style:`BOTH
    ~packing:(mainbox#pack ~expand:false) ()

(* ---------- top ---------- *)

let titlebox = 
  GPack.hbox ~homogeneous:true ~spacing:10 ~height:50 ~border_width:10
    ~packing:(mainbox#pack ~expand:false) ()

let visubox = GPack.hbox ~homogeneous:true ~packing:mainbox#add ()

let left_box = GPack.vbox ~homogeneous:false ~packing:visubox#add ()

let _ = GMisc.separator `VERTICAL ~packing:(visubox#pack ~expand:true) ()
  
let right_box = GPack.vbox ~homogeneous:false ~packing:visubox#add ()

let title = 
  GMisc.label ~text:"1. Choose a password" ~packing:titlebox#add ()
  
let arrow = 
  GMisc.arrow ~packing:titlebox#add ()

let title2 = 
  GMisc.label ~text:"2. Choose a file" ~packing:titlebox#add ()

let bboxleft = 
  GPack.button_box `VERTICAL ~spacing:10 ~layout:`EDGE ~border_width:10
    ~packing:(left_box#pack ~expand:false) ()

let bboxright =
  GPack.button_box `VERTICAL ~spacing:10 ~layout:`EDGE ~border_width:10
    ~packing:(right_box#pack ~expand:false) ()

(* ---------- bottom ---------- *)

let titlebox2 = 
  GPack.hbox ~homogeneous:true ~spacing:10 ~height:50 ~border_width:10
    ~packing:(mainbox#pack ~expand:false) ()

let visubox2 = GPack.hbox ~homogeneous:true ~packing:mainbox#add ()

let left_box2 = GPack.vbox ~homogeneous:false ~packing:visubox2#add ()

let _ = GMisc.separator `VERTICAL ~packing:(visubox2#pack ~expand:true) ()
  
let right_box2 = GPack.vbox ~homogeneous:false ~packing:visubox2#add ()

let title3 = 
  GMisc.label ~text:"1. Choose a file" ~packing:titlebox2#add ()
  
let arrow2 = 
  GMisc.arrow ~packing:titlebox2#add ()

let title4 = 
  GMisc.label ~text:"2. Password verification" ~packing:titlebox2#add ()

let bboxleft2 = 
  GPack.button_box `VERTICAL ~spacing:10 ~layout:`EDGE ~border_width:10
    ~packing:(left_box2#pack ~expand:false) ()

let bboxright2 =
  GPack.button_box `VERTICAL ~spacing:10 ~layout:`EDGE ~border_width:10
    ~packing:(right_box2#pack ~expand:false) ()

(* ---------- files and textfields ---------- *)

let pw = ref ""
let file = ref ""
let cfile = ref ""
let pwdecrypt = ref ""
let pwwrong = ref ""

let t_pw = 
  GMisc.label ~width:260 ~text:"" ~line_wrap:true ~packing:left_box#add ()
let t_file = 
  GMisc.label ~width:260 ~text:"" ~line_wrap:true ~packing:right_box#add ()
let t_cfile = 
  GMisc.label ~width:260 ~text:"" ~line_wrap:true ~packing:left_box2#add ()
let t_pw2 = 
  GMisc.label ~width:260 ~text:"" ~line_wrap:true ~packing:right_box2#add ()
let t_pw3 = 
  GMisc.label ~width:0 ~text:"" ()


let missing_file _ =
  let dlg = GWindow.message_dialog
    ~title:"Missing file"
    ~message:"<b><big>No file was imported!</big></b>"
    ~parent:window
    ~destroy_with_parent:true
    ~use_markup:true
    ~message_type:`ERROR
    ~position:`CENTER_ON_PARENT
    ~buttons:GWindow.Buttons.close () in
    let _ = dlg#run () in
      dlg#destroy ()

let wrong_file _ =
  let dlg = GWindow.message_dialog
    ~title:"Incorrect password"
    ~message:"<b><big>Incorrect password!</big>\nPlease try again.</b>"
    ~parent:window
    ~destroy_with_parent:true
    ~use_markup:true
    ~message_type:`ERROR
    ~position:`CENTER_ON_PARENT
    ~buttons:GWindow.Buttons.close () in
    let _ = dlg#run () in
      dlg#destroy ()


(* ---------- record buttons ---------- *)

external recordSound: int -> unit = "recordSound"

let sa _ =
  t_pw#set_text "Sound successfully recorded.";
  pw:="sounds/temp_e.wav";
  recordSound(0)

let sa_d _ =
  t_pw2#set_text "Sound successfully recorded.";
  pwdecrypt:="sounds/temp_d.wav";
  recordSound(1)

let sa_w _ =
  pwwrong:="sounds/temp_w.wav";
  recordSound(2)

let record_button =
  let rb = GButton.button ~stock:`MEDIA_RECORD ~packing:bboxleft#pack () in
    ignore(rb#connect#clicked ~callback:sa);
    rb

let record_button2 =
  let rb = GButton.button ~stock:`MEDIA_RECORD ~packing:bboxright2#pack () in
    ignore(rb#connect#clicked ~callback:sa_d);
    rb

(* ---------- open buttons ---------- *)

let default = function
  | None -> ""
  | Some v -> v

let sound_filter = GFile.filter
  ~name:"Sound File (*.wav)"
  ~patterns:(["*.wav";"*.mp3"]) () (* We can add the .mp3 extension *)

let vox_filter = GFile.filter
  ~name:"V.O.X. File (encrypted) (*.vox)"
  ~patterns:(["*.vox"]) ()

let no_filter = GFile.filter
  ~name:"All files (*.*)"
  ~patterns:(["*"]) ()

let file_dialog parent file label filter _ =
  let dialog = GWindow.file_chooser_dialog
    ~action:`OPEN
    ~destroy_with_parent:true
    ~title:"Import a file"
    ~width:800 
    ~height:600
    ~position:`CENTER_ON_PARENT
    ~parent () in
    dialog#set_filter filter;
    dialog#add_button_stock `CANCEL `CANCEL ;
    dialog#add_select_button_stock `OPEN `OPEN ;
    begin match dialog#run () with
      | `OPEN ->
	  file := (default dialog#filename);
	  label#set_text ("File path:\n"^(!file));
      | `DELETE_EVENT | `CANCEL -> ()
    end;
    dialog#destroy ()

external previewSound: string -> unit = "previewSound"

let playfile _ =
  if(!pw = "") then missing_file ()
    else (previewSound !pw); ()

let b_play =
  let button = GButton.button
    ~stock:`MEDIA_PLAY
    ~packing:bboxleft#pack () in
  ignore(button#connect#clicked ~callback:playfile);
  button

let b_sound_import =
  let button = GButton.button
    ~stock:`OPEN
    ~packing:bboxleft#pack () in
  ignore(button#connect#clicked ~callback:(file_dialog window pw t_pw sound_filter));
  button

let b_file_to_crypt =
  let button = GButton.button
    ~stock:`OPEN
    ~packing:bboxright#pack () in
  ignore(button#connect#clicked ~callback:(file_dialog window file t_file no_filter));
  button

let b_file_to_decrypt =
  let button = GButton.button
    ~stock:`OPEN
    ~packing:bboxleft2#pack () in
  ignore(button#connect#clicked ~callback:(file_dialog window cfile t_cfile vox_filter));
  button

let b_password_verification =
  let button = GButton.button
    ~stock:`OPEN
    ~packing:bboxright2#pack () in
  ignore(button#connect#clicked ~callback:(file_dialog window pwdecrypt t_pw2 sound_filter));
  button

(* ---------- record wrong popup ---------- *)

external encryption: string -> string -> string -> unit = "encrypt"

let record_wrong _ = 
        pwwrong := "";
	let test = !pwwrong in
	let rec dlg = GWindow.dialog () 
	~title: "Record a different word"
	~modal: true 
 	~resizable:false
	~parent: window 
	~position: `CENTER in
	ignore (GMisc.label () 
	~text:"\nFor a correct analysis, you are required\nto record a completely different word.\n\nRecord a word or open a file.\nThe encryption will then begin.\n"
	~packing: dlg#vbox#add);
	dlg#add_button_stock (`OPEN) `OPEN;
	dlg#add_button_stock (`MEDIA_RECORD) `MEDIA_RECORD;
	dlg#add_button_stock (`CANCEL) `CANCEL;

	dlg#set_default_response `MEDIA_RECORD;
	begin 
		match dlg#run() with
		|  `OPEN -> (file_dialog window pwwrong t_pw3 sound_filter ());
		if not (test = !pwwrong) then begin
			(encryption !file !pw !pwwrong); end; 
			dlg#destroy (); () 
		|  `MEDIA_RECORD -> dlg#destroy (); (sa_w ()); 
		(encryption !file !pw !pwwrong); ()
		|  _ -> dlg#destroy (); ()
	end;  
	dlg#destroy (); ()

(* ---------- encrypt/decrypt buttons ---------- *)

external decryption: string -> string -> bool = "decrypt"

let encrypt _ =
  if(!file = "" || !pw = "") then missing_file ()
    else record_wrong() ; ()

let decrypt _ =
  if(!pwdecrypt = "" || !cfile = "") then missing_file ()
    else if not (decryption !cfile !pwdecrypt) then wrong_file ()
    (* else Popup with error concerning the password error *) 

(* Le chemin du mot de passe pour déchiffrer est donné par !pwdecrypt *)

let encrypt_button =
  let btn = GButton.button 
    ~label:"Encrypt"
    ~packing:bboxright#add () in
      ignore(GMisc.image ~stock:`EXECUTE ~packing:btn#set_image ());
      ignore(btn#connect#clicked ~callback:encrypt);
      btn

let decrypt_button =
  let btn = GButton.button 
    ~label:"Decrypt"
    ~packing:bboxright2#add () in
      ignore(GMisc.image ~stock:`EXECUTE ~packing:btn#set_image ());
      ignore(btn#connect#clicked ~callback:decrypt);
      btn

(* ---------- desactivation ---------- *)

let switch_to_encrypt () =
  titlebox#misc#set_sensitive true;
  visubox#misc#set_sensitive true;
  titlebox2#misc#set_sensitive false;
  visubox2#misc#set_sensitive false
  
let switch_to_decrypt () =
  titlebox#misc#set_sensitive false;
  visubox#misc#set_sensitive false;
  titlebox2#misc#set_sensitive true;
  visubox2#misc#set_sensitive true

let _ = switch_to_encrypt ()

let modes =
  let menu = GMenu.menu () in
    let i1 = 
      GMenu.menu_item ~label:"Encrypt a file" ~packing:menu#add () in
    let i2 = 
      GMenu.menu_item ~label:"Decrypt a file" ~packing:menu#add () in
        ignore(i1#connect#activate ~callback:switch_to_encrypt);
        ignore(i2#connect#activate ~callback:switch_to_decrypt);
  menu

let change_mode =
  let b = GButton.menu_tool_button
    ~label:"Mode"
    ~menu:modes
    ~packing:toolbar#insert () in
    ignore(b#set_stock_id `PROPERTIES);
    b

(* ---------- about window ---------- *)

let _ = GButton.separator_tool_item ~packing:toolbar#insert ()

let about_button =
  let dlg = GWindow.about_dialog
    ~name:"V.O.X."
    ~authors:["The Sandmen"]
    ~comments:"Security software using voice-based passwords."
    ~version:"1.3"
    ~website:"http://vox.damwdan.com/"
    ~website_label:"vox.damwdan.com"
    ~position:`CENTER_ON_PARENT
    ~parent:window
    ~destroy_with_parent:true () in
  let btn = GButton.tool_button ~stock:`ABOUT ~packing:toolbar#insert () in
  ignore(btn#connect#clicked (fun () -> ignore (dlg#run ()); dlg#misc#hide ()));
  btn

(* ---------- quit button --------- *)

let _ = GButton.separator_tool_item ~packing:toolbar#insert ()

let confirm _ =
  let dlg = GWindow.message_dialog
    ~title:"Warning"
    ~message:"<b><big>Do you really want to quit?</big></b>\n"
    ~parent:window
    ~destroy_with_parent:true
    ~use_markup:true
    ~message_type:`QUESTION
    ~position:`CENTER_ON_PARENT
    ~buttons:GWindow.Buttons.yes_no () in
    let res = dlg#run () = `NO in
      dlg#destroy ();
      res

let _quit _ = if not(confirm ()) then GMain.quit () else ()

let mainquit _ =
  let res = confirm() in
    if not(res) then begin GMain.quit(); res; end else res

let quit =
  let button = GButton.tool_button ~stock:`QUIT ~packing:toolbar#insert () in
    ignore(button#connect#clicked ~callback:_quit);
    button

(* --------- main --------- *)

let graphical () =
    begin
    ignore(window#event#connect#delete mainquit);
    window#show ();
    GMain.main ();
    end

external recordConsole: int -> unit = "sample_console"
let console_pass mode = 
begin
print_endline("\n
    Would you like to chose a file (-f) or record a sound (-r)?");
    match (read_line()) with
        | "-f" -> 
            begin
            print_endline("Enter the password's path:");
            match mode with 
            | 0 -> pw := read_line(); false
            | 1 -> pwdecrypt := read_line(); false
            | _ -> pwwrong := read_line(); false
            end
        | "-r" -> 
            begin
            recordConsole(mode);
            print_newline();
            match mode with 
            | 0 -> pw := "temp_e.wav"; false
            | 1 -> pwdecrypt := "temp_d.wav"; false
            | _ -> pwwrong := "temp_w.wav"; false
            end
        | _ -> true
end
let console_encrypt isEncrypting =
begin
print_endline("\n
    Enter the filepath:");
    if (isEncrypting) then
        file := read_line()
    else
        cfile := read_line()
end
        
let console () =
begin
    let loop = ref true in
    while (!loop) do
        print_endline("Would you like to encrypt (-e) or decrypt (-d) a file?");
        match (read_line()) with
        | "-e" ->
          begin
                while (!loop) do
                    print_string("=== Password choice ===");
                    loop := console_pass 0;
                    print_newline();
                done;
                print_string("=== File to encrypt ===");
                console_encrypt true;
                print_newline();
                
                loop := true;
                while (!loop) do
                    print_string("=== Wrong Password choice ===");
                    loop := console_pass 2;
                    print_newline();
                done;
                encryption !file !pw !pwwrong;
                
                print_endline("Would you like to quit?");
                match (read_line()) with
                | "y" | "yes" -> loop := false;
                | _ -> loop := true;
                
          end
        | "-d" ->
          begin
            print_string("=== File to decrypt ===");
            console_encrypt false;
            print_newline();
            while (!loop) do
                    print_string("=== Password choice ===");
                    loop := console_pass 1;
                    print_newline();
                done;
                loop := not (decryption !cfile !pwdecrypt);
          end
        | _ -> loop := true
    done
end

let _ =
    let message = "
\nUsing V.O.X.:
    -vox (graphical mode) [-g ; --graphical ; --gui]
    -vox -nw (guided console mode, with recording) [--nowindow]
    -vox -e file audiopass audiowrongpass (encryption, console mode) [--encrypt]
    -vox -d file.vox audiopass (decryption, console mode) [--decrypt]
    -vox -u (updates the software) [--update]
    -vox -v (displays the version of VOX you're using) 
    -vox -help (displays this message)\n\n" in 
                
    match (Array.length Sys.argv) with
        | x when x < 2 -> graphical ()
        | 2 -> begin
               match Sys.argv.(1) with
                 | "-g" | "--graphical" | "--gui" -> graphical ()
                 | "-help" | "--help" -> print_endline(message)
                 | "-nw" | "--nowindow" -> console ()
                 | _ -> invalid_arg message
               end
        | 4 -> if (Sys.argv.(1) = "-d" || Sys.argv.(1) = "--decrypt") then
                    ignore (decryption Sys.argv.(2) Sys.argv.(3))
               else
                    invalid_arg message
        | 5 -> if (Sys.argv.(1) = "-e" || Sys.argv.(1) = "--encrypt") then
                    encryption Sys.argv.(2) Sys.argv.(3) Sys.argv.(4)
               else
                    invalid_arg message
        | _ -> invalid_arg message
