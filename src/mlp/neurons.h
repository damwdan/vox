/*
 * Neuronal network
 * Project: V.O.X.
 * Developer: TheSandmen
 */

#define _XOPEN_SOURCE 500
#include <unistd.h>

#include "../recording/record.h"

#include <math.h> //sigmoid() and fabs()
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../inc/fmod.h"
#include "../inc/fmod_errors.h"
#include "../inc/wincompat.h"

#include <caml/alloc.h>
#include <caml/mlvalues.h>
#include <caml/memory.h>

#include <string.h>

#include <time.h> // Pour le random

#define ItNum 6000
#define inputN 1024
#define outputN 2
#define hiddenN 64
#define hiddenL 4

#define WEIGHT_SIZE inputN*hiddenN+(hiddenN*hiddenN*(hiddenL-1))+hiddenN*outputN

/*helper macros to locate the appropriate weight at the weights vector*/
#define inputToHidden(inp,hid)                weights[(inputN*hid+inp)]
#define hiddenToHidden(toLayer,fromHid,toHid)   weights[(inputN*hiddenN+ ((toLayer-2)*hiddenN*hiddenN)+hiddenN*fromHid+toHid)]
#define hiddenToOutput(hid,out)               weights[(inputN*hiddenN + (hiddenL-1)*hiddenN*hiddenN + hid*outputN+out)]


/*Helper macros just as above, but for the previous Weights*/
#define _prev_inputToHidden(inp,hid)                prWeights[(inputN*hid+inp)]
#define _prev_hiddenToHidden(toLayer,fromHid,toHid)   prWeights[(inputN*hiddenN+ ((toLayer-2)*hiddenN*hiddenN)+hiddenN*fromHid+toHid)]
#define _prev_hiddenToOutput(hid,out)               prWeights[(inputN*hiddenN + (hiddenL-1)*hiddenN*hiddenN + hid*outputN+out)]


/*helper macro to locate the appropriate hidden neuron*/
#define hiddenAt(layer,hid)                     hiddenNeurons[(layer-1)*hiddenN + hid]

/*helper macros to locate the appropriate neuron's delta*/
#define outputDeltaAt(out)                      (*(odelta+out))
#define hiddenDeltaAt(layer,hid)                (*(hdelta+(layer-1)*hiddenN+hid))

/*math help*/
#define sigmoid(value)  (1/(1+exp(-value)));
#define dersigmoid(value) (value*(1-value))

double my_random(double a, double b);
void init_weights(double *weights, double *pr_weights);
void load_weights(double *weights, double *pr_weights);

void train_network(char* pass, char* wrong, double *weights);
int full_network(char* filepath, double *weights);
