/*
 * Neuronal network
 * Project: V.O.X.
 * Developer: TheSandmen
 */

#include "neurons.h"

char* tostr(int b)
{
  if (b)
    return "true";
  else
    return "false";
}

double my_random(double a, double b) {
  return (rand()/(double)RAND_MAX ) * (b-a) + a;
}

/* If weights does not exist, creates it */
void init_weights(double *weights, double *pr_weights)
{
  printf("Creating weights...\n");
  srand(time(NULL)); // initialisation de rand
  for (unsigned i = 0; i < WEIGHT_SIZE; i++)
    {
      weights[i] = my_random(-0.5,0.5);
      if (pr_weights != NULL)
        pr_weights[i] = weights[i];
    }
}

/* Loads the weights from the file */
void load_weights(double *weights, double *pr_weights)
{
  FILE *fp = fopen("weights", "r");
  if (fp)
    {
      printf("Loading weights...\n");
      for (unsigned i = 0; i < WEIGHT_SIZE; i++) {
        char line[10] = "";
        int c = 0;

        while ((c = fgetc(fp)) != '\n' && c != EOF)
          sprintf(line, "%s%c", line, c);

        weights[i] = atof(line);
        if (pr_weights != NULL)
          pr_weights[i] = weights[i];
      }
      fclose(fp);
    }
  else
    init_weights(weights, pr_weights);
}

void save_weights (double *weights)
{
  FILE *fp = fopen("weights", "w+");

  for (unsigned i = 0; i < WEIGHT_SIZE; i++)
    fprintf(fp, "%f\n", weights[i]);
  fclose(fp);
}

void populateInput(char *filepath, double *inputNeurons)
{
  if (!strcmp("recorded", filepath))
    filepath = "sounds/temp.wav";

  float noise[inputN];
  for (int i = 0; i < inputN; i++)
    noise[i] = 0;

  FMOD_SYSTEM *system;
  FMOD_SOUND *sound;
  FMOD_CHANNEL *channel = 0;
  FMOD_RESULT result;
  int key;

  result = FMOD_System_Create(&system);
  ERRCHECK(result);

  result = FMOD_System_Init(system, 32, FMOD_INIT_NORMAL, NULL);
  ERRCHECK(result);

  /* Opens sound file */
  result = FMOD_System_CreateSound(
                                   system
                                   , filepath
                                   , FMOD_SOFTWARE
                                   , 0
                                   , &sound);

  ERRCHECK(result);

  result = FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, sound, 0, &channel);
  result = FMOD_Channel_SetMute(channel, 1);
  int aux = 0;

  int i = 0;
  while (i < inputN)
    inputNeurons[i++] = 0;
  int precCorrected = 1;
  do
    {
      unsigned int ms = 0;
      unsigned int lenms = 0;
      int playing = 0;

      if (channel)
        {
          FMOD_SOUND *currentsound = 0;

          result = FMOD_Channel_IsPlaying(channel, &playing);
          ERRCHECK(result);

          result = FMOD_Channel_GetPosition(channel, &ms, FMOD_TIMEUNIT_MS);
          ERRCHECK(result);

          FMOD_Channel_GetCurrentSound(channel, &currentsound);
          if (currentsound)
            {
              result = FMOD_Sound_GetLength(currentsound, &lenms, FMOD_TIMEUNIT_MS);
              ERRCHECK(result);
            }


          // Gets spectrum
          if (ms % 25 == 0)
            {
              int playpos;
              FMOD_Channel_GetPosition(channel, &playpos, FMOD_TIMEUNIT_MS);
              if (playpos < NOISE_DELAY)
                {
                  float temp[inputN];
                  FMOD_Channel_GetSpectrum(channel, temp, inputN, 0,  FMOD_DSP_FFT_WINDOW_RECT);
                  for (int i = 0; i < inputN; i++) {
                    noise[i] += temp[i];
                  }
                }
              else
                {

                  float spectrum[inputN];
                  FMOD_Channel_GetSpectrum(channel, spectrum, inputN, 0,  FMOD_DSP_FFT_WINDOW_HAMMING);
                  int i = 0;
                  while (i < inputN)
                    inputNeurons[i] += (double)(noise_band(spectrum[i],noise[i++], &precCorrected));
                }
            }
        }

      result = FMOD_Sound_GetLength(sound, &lenms, FMOD_TIMEUNIT_MS);
      ERRCHECK(result);

      printf("Time %02d:%02d:%02d/%02d:%02d:%02d - %s\r",
             ms / 1000 / 60, ms / 1000 % 60, ms / 10 % 100,
             lenms / 1000 / 60, lenms / 1000 % 60, lenms / 10 % 100,
             playing ? "Playing" : "Stopped");

      aux = playing;

      fflush(stdout);

      Sleep(1);

    } while (aux);

  result = FMOD_Sound_Release(sound);
  ERRCHECK(result);
  result = FMOD_System_Close(system);
  ERRCHECK(result);
  result = FMOD_System_Release(system);
  ERRCHECK(result);
}

int network(char *path, int target, int learning, double *inputNeurons, double *weights, double *prWeights)
{
  float momentum = 0.6;
  float teachingStep = 0.45;
  float mse = 999.0;
  float error = 0.0;
  double hiddenNeurons[hiddenN*hiddenL];
  double outputNeurons[outputN];
  double tempWeights[WEIGHT_SIZE];

  //the delta of the output layer
  float odelta[outputN];
  //the delta of each hidden layer
  float hdelta[hiddenN*hiddenL];

  float sum = 0;
  int result = -1;

  // -------------------------------------------------------- //
  // ------------------NETWORK CALCULATION------------------- //
  // -------------------------------------------------------- //


  for(int hidden = 0; hidden < hiddenN; hidden++)
    {
      hiddenAt(1,hidden) = 0;
      for(int input = 0 ; input < inputN; input ++)
        {
          hiddenAt(1,hidden) += inputNeurons[input] * inputToHidden(input,hidden);
        }
      hiddenAt(1,hidden) = sigmoid(hiddenAt(1,hidden));
    }

  for(int i = 2; i <= hiddenL; i ++)
    {
      for(int j = 0; j < hiddenN; j++)
        {
          hiddenAt(i,j) = 0;
          for(int k = 0; k < hiddenN; k++)
            {
              hiddenAt(i,j) += hiddenAt(i-1,k)*hiddenToHidden(i,k,j);
            }
          hiddenAt(i,j) = sigmoid(hiddenAt(i,j));
        }
    }


  int i;
  for(i =0; i< outputN; i ++)
    {
      outputNeurons[i] = 0;
      for(int j = 0; j <hiddenN; j++)
        {
          outputNeurons[i] += hiddenAt(hiddenL,j) * hiddenToOutput(j,i);
        }
      outputNeurons[i] = sigmoid(outputNeurons[i] );
    }

  // -------------------------------------------------------- //
  // ------------------------TARGETING----------------------- //
  // -------------------------------------------------------- //

  if (!learning)
    {
      float value = -1;
      for (int i = 0; i < outputN; i++)
        {
          sum += outputNeurons[i];
          if (outputNeurons[i] > value)
            {
              result = i;
              value = outputNeurons[i];
            }
          printf("value: %f -> %s\n", outputNeurons[i], tostr(i));
        }
      printf("========== Result for %s:\n", path);
      printf("========== Sum: %f ---- %s ----\n\n", sum, tostr(result));
    }
  // -------------------------------------------------------- //
  // ------------------------TRAINING------------------------ //
  // -------------------------------------------------------- //
  else
    {
      // -------------CALCULATION OF THE DELTA-------------- //
      for(int i = 0; i < outputN; i ++)
        {
          if(i != target)
            {
              outputDeltaAt(i) = (0.0 - outputNeurons[i])*dersigmoid(outputNeurons[i]);
              error += (0.0 - outputNeurons[i])*(0.0-outputNeurons[i]);
            }
          else
            {
              outputDeltaAt(i) = (1.0 - outputNeurons[i])*dersigmoid(outputNeurons[i]);
              error += (1.0 - outputNeurons[i])*(1.0-outputNeurons[i]);
            }
        }

      // ------------------RETROPROPAGATION------------------- //
      for(int i = 0; i < hiddenN; i++)
        {
          hiddenDeltaAt(hiddenL,i) = 0;
          for(int j = 0; j < outputN; j ++)
            hiddenDeltaAt(hiddenL,i) += outputDeltaAt(j) * hiddenToOutput(i,j) ;

          hiddenDeltaAt(hiddenL,i) *= dersigmoid(hiddenAt(hiddenL,i));
        }

      for(int i = hiddenL-1; i >0; i--)
        {
          for(int j = 0; j < hiddenN; j ++)

            {
              hiddenDeltaAt(i,j) = 0;
              for(int k = 0; k <hiddenN; k++)
                hiddenDeltaAt(i,j) +=  hiddenDeltaAt(i+1,k) * hiddenToHidden(i+1,j,k);

              hiddenDeltaAt(i,j) *= dersigmoid(hiddenAt(i,j));
            }
        }


      // ----------------------WEIGHT MODIFICATION-------------------- //

      for (int i = 0; i < WEIGHT_SIZE; i++)
        {
          tempWeights[i] = weights[i];
        }

      for(int i = 0; i < inputN; i ++)
        {
          for(int j = 0; j < hiddenN; j ++)
            {
              inputToHidden(i,j) +=   momentum*(inputToHidden(i,j) - _prev_inputToHidden(i,j)) + teachingStep* hiddenDeltaAt(1,j) * inputNeurons[i];
            }
        }
      for(int i = 2; i <=hiddenL; i++)
        {
          for(int j = 0; j < hiddenN; j ++)
            {
              for(int k =0; k < hiddenN; k ++)
                {
                  hiddenToHidden(i,j,k) += momentum*(hiddenToHidden(i,j,k) - _prev_hiddenToHidden(i,j,k)) + teachingStep * hiddenDeltaAt(i,k) * hiddenAt(i-1,j);
                }
            }
        }

      for(int i = 0; i < outputN; i++)
        {
          for(int j = 0; j < hiddenN; j ++)
            {
              hiddenToOutput(j,i) += momentum*(hiddenToOutput(j,i) - _prev_hiddenToOutput(j,i)) + teachingStep * outputDeltaAt(i) * hiddenAt(hiddenL,j);
            }
        }

      for (int i = 0; i < WEIGHT_SIZE; i++)
        {
          prWeights[i] = tempWeights[i];
        }

      mse += error/(outputN+1);

      //zero out the error for the next iteration
      error = 0;
    }
  return result;
}

void train_network(char* pass, char* wrong, double *weights)
{
  printf("\n");
  printf("File used for encryption: %s\nFile used as wronged: %s\n", pass,wrong);

  printf("Starting training...\n");
  double pr_weights[WEIGHT_SIZE];
  init_weights(weights, pr_weights);

  printf("\n");
  double passNeurons[inputN];
  populateInput(pass, passNeurons);
  double wrongNeurons[inputN];
  populateInput(wrong, wrongNeurons);
  printf("\n");

  for (int j = 0; j <= ItNum; j++)
    {
      printf("Iterating... %d/%d\r", j, ItNum);
      fflush(stdout);

      network(pass, 1, 1, passNeurons, weights, pr_weights);
      network(wrong, 0, 1, wrongNeurons, weights, pr_weights);
    }
  printf("\n");
}

int full_network(char* filepath, double *weights)
{
  printf("\n");
  double passNeurons[inputN];
  populateInput(filepath, passNeurons);
  printf("\n");

  int res = network(filepath, 0, 0, passNeurons, weights, NULL);
  return res;
}
