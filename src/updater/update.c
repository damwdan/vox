/*
* V.O.X. updater
* Project: V.O.X.
* Developer: TheSandmen
*/

#include "update.h"

static size_t WriteCallBack(void* content, size_t size, size_t nmemblocks, void* userp)
{
    size_t realsize = size*nmemblocks;
    MemoryChunk* mem = (MemoryChunk*)userp;

    mem->memory= realloc(mem->memory, mem->size + realsize + 1);
    if (mem->memory == NULL) /* out of memory */
    {
        printf("Out of memory");
        return 0;
    }

    memcpy(&(mem->memory[mem->size]), content, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

int getOnlineVersion(char** version)
{
    CURL *curl_handle;
    CURLcode result;

    MemoryChunk chunk;

    chunk.memory = malloc(1); /* realloc will change the size in above func */
    chunk.size = 0; /* empty */

    /* to be called before any curl functions*/
    curl_global_init(CURL_GLOBAL_ALL);
    /* init the curl session */
    curl_handle = curl_easy_init();

    /* sets the url to read */
    curl_easy_setopt(curl_handle, CURLOPT_URL, "http://uploads.damwdan.com/vox/version");

    /* WriteCallBack will be called as soon as there is data recieved
    * that needs to be saved.*/
    curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, WriteCallBack);

    /* data recieved by the callback will be saved in the chunk */
    curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void*)&chunk);

    /* specifies an user agent to avoid being rejected by the server */
    curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    /* actually gets the data */
    result = curl_easy_perform(curl_handle);

    /* checks if perform went okay, writes error in stderr if didn't
    * if result is okay, else must contain the things to do to chunk.memory
    * which contains the remote file. */
    if (result != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n", curl_easy_strerror(result));
        return 0;
    }
    else
    {
        printf("Remote version: %s\n", chunk.memory);
        strcpy(*version, chunk.memory);
    }

    /* pretty self-explanatory */
    curl_easy_cleanup(curl_handle);
    if (chunk.memory)
    free(chunk.memory);

    /* cleanup of the global curl */
    curl_global_cleanup();

    return 1;
}

int main()
{
    char* version = malloc(8 * sizeof(char));
    getOnlineVersion(&version);
    printf("Current version: %s\n", CURRENT_VERSION);

    if (strcmp(version, CURRENT_VERSION))
    {
        printf("Update is required.\n");
        char* argv[] = {"update.sh", NULL};
        execve("", argv, NULL);
    }

    printf("Update went wrong.\n");

    return 1;
}
