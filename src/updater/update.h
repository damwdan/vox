/*
 * V.O.X. updater
 * Project: V.O.X.
 * Developer: TheSandmen
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <curl/curl.h>

#define CURRENT_VERSION "2.0"

typedef struct MemoryStruct MemoryChunk;
struct MemoryStruct {
    char *memory;
    size_t size;
};

