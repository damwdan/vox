#!/bin/sh

wget_out=$(wget -q "http://uploads.damwdan.com/vox/vox-latest.tar.gz")
if [ $? -ne 0 ]; then
    echo "Update could not process because the remote version cannot be found."
    exit
else
    tar xf vox-latest.tar.gz
    cd vox-latest
    make
    make install
    cd ..
    rm -rf vox-latest*
    /usr/local/bin/vox
fi
