/*
* ROT Encryption.
* Project: V.O.X.
* Developer: TheSandmen
*/

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "rot.h"

void rot_encryption(char* message, size_t value, size_t messlen)
{
    size_t i;
    for (i = 0; i < messlen; ++i)
    {
        message[i] = message[i] + value;
    }
}

void rot_decryption(char* message, size_t value, size_t messlen)
{
    size_t i;
    for (i = 0; i < messlen; ++i)
    {
        message[i] = message[i] - value;
    }
}

char rot_echar(char c, size_t value)
{
    return (c + value);
}

char rot_dchar(char c, size_t value)
{
    return (c - value);
}

void rot_test(char* message, size_t value)
{
    size_t l = strlen(message);

    printf("Message is: %s\n", message);
    printf("Value is: %lu\n", value);

    printf("Original decimal message is: ");
    size_t i;
    for (i = 0; i < l; ++i)
    {
        printf("<%i>", message[i]);
    }
    printf("\n");

    rot_encryption(message, value, l);

    printf("Size is: %zu\n", l);

    printf("Encrypted decimal message is: ");
    for (i = 0; i < l; ++i)
    {
        printf("<%i>", message[i]);
    }
    printf("\n");

    rot_decryption(message, value, l);
    printf("Redecrypted message is: %s\n(should be the same as message)\n",
    message);
}

void rot_encrypt(char* input_name, size_t value)
{
    printf("Your file is being encrypted...\n\n");

    char output_name[1024];
    strcpy(output_name, input_name);
    strcat(output_name, ".vox");
    FILE *input = fopen(input_name, "r");
    FILE *output = fopen(output_name, "w");

    char c;

    while ((c = fgetc(input)) != EOF)
    fputc(rot_echar(c, value), output);

    char backup[1024];
    strcpy(backup, input_name);
    strcat(backup, ".bak");
    rename(input_name, backup);

    printf("Your file has successfully been encrypted.\nThe output was written in %s.\n", output_name);
}

void rot_decrypt(char* input_name, size_t value)
{
    printf("Your file is being decrypted...\n\n");

    char output_name[1024];
    strcpy(output_name, input_name);
    output_name[strlen(input_name) - 4] = 0;
    FILE *input = fopen(input_name, "r");
    FILE *output = fopen(output_name, "w");

    char c;

    while ((c = fgetc(input)) != EOF)
    fputc(rot_dchar(c, value), output);

    printf("Your file has successfully been decrypted.\nThe output was written in %s.\n", output_name);
}

/*const char *extension(const char *filename)
{
    const char *ext = strrchr(filename, '.');
    if(!ext || ext == filename)
        return "";

    return ext + 1;
}

int main(int argc, char* argv[])
{
    if (argc < 4)
    {
        printf("Missing arguments.\nrot must be called with -t, -e or -d and two parameters.\n");
        return 1;
    }

    if (!strcmp(argv[1],"-t"))
    {
        rot_test(argv[2], (size_t)atoi(argv[3]));
        return 0;
    }
    else if (!strcmp(argv[1],"-e"))
    {
        rot_encrypt(argv[2], (size_t)atoi(argv[3]));
        return 0;
    }
    else if (!strcmp(argv[1],"-d"))
    {
        const char *ext = extension(argv[2]);
        if (!strcmp(ext, "") || strcmp(ext, "vox"))
        {
            printf("This file is not a .vox file.\n");
            return 1;
        }
        rot_decrypt(argv[2], (size_t)atoi(argv[3]));
        return 0;
    }
    else
    {
        printf("Unknown command %s\n", argv[1]);
        return 1;
    }
}*/
