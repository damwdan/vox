/*
* ROT Encryption.
* Project: V.O.X.
* Developer: TheSandmen
*/

/* README
* Run rot -t <message> <value> to print a ROT encryption test.
* Run rot -e <message> <value> to encrypt the message using the value.
* Run rot -d <message> <value> to decrypt the message using the value.
*/

/*
* Applies ROT encryption to message using value.
* This function was implemented for testing purposes only.
*/
void rot_encryption(char* message, size_t value, size_t messlen);

/*
* Applies ROT decryption to message using value.
* This function was implemented for testing purposes only.
*/
void rot_decryption(char* message, size_t value, size_t messlen);

/*
* Returns the new ROT encrypted c using value.
*/
char rot_echar(char c, size_t value);

/*
* Returns the new ROT encrypted c using value.
*/
char rot_dchar(char c, size_t value);

/*
* Applies ROT encryption to the file input_name using value.
* encrypt will rename the original file with the .bak extension.
* encrypt will write the encrypted file with the .vox extension.
*/
void rot_encrypt(char* input_name, size_t value);

/*
* Applies ROT decryption to the file input_name using value.
* decrypt will not remove the encrypted file.
* decrypt will write the decrypted file with the original name.
*/
void rot_decrypt(char* input_name, size_t value);

/*
* Prints a ROT test.
*/
void rot_test(char* message, size_t value);
