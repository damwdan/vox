/*
* XOR Encryption.
* Project : V.O.X
* Developer: TheSandmen
*/

#include <stdio.h>
#include <unistd.h>
#include <string.h>

#include "xor.h"


/*void xor_encryption(char* message, char* key, size_t messlen)
{
    size_t keylen = strlen(key);
    size_t i;
    for (i = 0; i < messlen; ++i)
    {
        message[i] = message[i] ^ key[i % keylen];
    }
}*/

char xor_char(char c, char* key, size_t index)
{
    size_t keylen = strlen(key);

    return (c ^ key[index % keylen]);
}

/*void xor_test(char* message, char* key)
{
    size_t l = strlen(message);

    printf("Message is: %s\n", message);
    printf("Key is: %s\n", key);

    printf("Original decimal message is: ");
    size_t i;
    for (i = 0; i < l; ++i)
    {
        printf("<%i>", message[i]);
    }
    printf("\n");

    xor_encryption(message, key, l);

    printf("Size is: %zu\n", l);

    printf("Encrypted decimal message is: ");
    for (i = 0; i < l; ++i)
    {
        printf("<%i>", message[i]);
    }
    printf("\n");

    xor_encryption(message, key, l);
    printf("Redecrypted message is: %s\n(should be the same as message)\n",
    message);
}

void xor_encrypt(char* input_name, char* key)
{
    printf("Your file is being encrypted...\n\n");

    char output_name[1024];
    strcpy(output_name, input_name);
    strcat(output_name, ".vox");
    FILE *input = fopen(input_name, "r");
    FILE *output = fopen(output_name, "w");

    char c;

    while ((c = fgetc(input)) != EOF)
    fputc(xor_echar(c, key), output);

    char backup[1024];
    strcpy(backup, input_name);
    strcat(backup, ".bak");
    rename(input_name, backup);

    printf("Your file has successfully been encrypted.\nThe output was written in %s.\n", output_name);
}

void xor_decrypt(char* input_name, char* key)
{
    printf("Your file is being decrypted...\n\n");

    char output_name[1024];
    strcpy(output_name, input_name);
    output_name[strlen(input_name) - 4] = 0;
    FILE *input = fopen(input_name, "r");
    FILE *output = fopen(output_name, "w");

    char c;

    while ((c = fgetc(input)) != EOF)
    fputc(xor_dchar(c, key), output);

    printf("Your file has successfully been decrypted.\nThe output was written in %s.\n", output_name);
}

const char *extension(const char *filename)
{
    const char *ext = strrchr(filename, '.');
    if(!ext || ext == filename)
        return "";

    return ext + 1;
}

int main(int argc, char* argv[])
{
    if (argc < 4)
    {
        printf("Missing arguments.\nxor must be called with -t, -e or -d and two parameters.\n");
        return 1;
    }

    if (!strcmp(argv[1],"-t"))
    {
        xor_test(argv[2], argv[3]);
        return 0;
    }
    else if (!strcmp(argv[1],"-e"))
    {
        xor_encrypt(argv[2], argv[3]);
        return 0;
    }
    else if (!strcmp(argv[1],"-d"))
    {
        const char *ext = extension(argv[2]);
        if (!strcmp(ext, "") || strcmp(ext, "vox"))
        {
            printf("This file is not a .vox file.\n");
            return 1;
        }
        xor_decrypt(argv[2], argv[3]);
        return 0;
    }
    else
    {
        printf("Unknown command %s\n", argv[1]);
        return 1;
    }
}*/
