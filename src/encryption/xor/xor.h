/*
* XOR Encryption.
* Project: V.O.X.
* Developer: TheSandmen
*/

/* README
* Run xor -t <message> <key> to print a XOR encryption test.
* Run xor -e <message> <key> to encrypt the message using the key.
* Run xor -d <message> <key> to decrypt the message using the key.
*/

/*
* Applies XOR encryption to message using key.
* This function was implemented for testing purposes only.
*/
void xor_encryption(char* message, char* key, size_t messlen);

/*
* Returns the new XOR encrypted  c using key.
*/
char xor_char(char c, char* key, size_t index);

/*
* Applies XOR encryption to the file input_name using key.
* encrypt will rename the original file with the .bak extension.
* encrypt will write the encrypted file with the .vox extension.
*/
void xor_encrypt(char* input_name, char* key);

/*
* Applies XOR decryption to the file input_name using key.
* decrypt will remove the encrypted file.
* decrypt will write the decrypted file with the original name.
*/
void xor_decrypt(char* input_name, char* key);

/*
* Prints a XOR test.
*/
void xor_test(char* message, char* key);
