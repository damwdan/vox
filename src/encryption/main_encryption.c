/*
* XOROT Encryption.
* Project: V.O.X.
* Developer: TheSandmen
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <err.h>
#include <math.h>
#include "main_encryption.h"
#include "sha1/sha1.h"
#include "../mlp/neurons.h"


const char *extension(const char *filename)
{
    const char *ext = strrchr(filename, '.');
    if(!ext || ext == filename)
    return "";

    return ext + 1;
}

int make_key(const double* weights, size_t size, char* key)
{
    double fractionnal, integral;

    for (size_t i = 0; i < size; i++)
    {
        fractionnal = modf(weights[i], &integral);
        modf(fractionnal * 100, &integral);
        integral = labs(integral);
        integral = ((int)integral % 26) + 97;
        key[i] = (char)integral;
    }

    key[size] = 0;

    return 1;
}

int make_value(const double* weights, size_t size, size_t max_size)
{
    if (size > max_size)
    errx(EXIT_FAILURE, "Too much elements.");

    double fractionnal, integral;
    int result = 0;

    for (size_t i = 0; i < size; i++)
    {
        fractionnal = modf(weights[i], &integral);
        modf(fractionnal * 100, &integral);
        integral = labs(integral);
        result += (int)integral;
    }

    return result % 120;
}

void encrypt(char* input_name, char* pass, char* fake)
{
    printf("\n");

    char output_name[1024];
    strcpy(output_name, input_name);
    strcat(output_name, ".vox");

    /* ---------- Training the ann --------- */

    printf("Training the artificial neural network.\n");

    double weights[WEIGHT_SIZE];
    train_network(pass, fake, weights);

    printf("Neural network was successfully trained.\n");
    printf("%1.15f\n", weights[7]);
    printf("%1.15f\n", weights[0]);
    printf("%1.15f\n", weights[8]);
    printf("%1.15f\n", weights[1]);

    /* ---------- Generating hash ---------- */

    printf("Generating the SHA1 hash.\n");

    unsigned char hash[SHA_DIGEST_LENGTH];

    if (sha1_double(weights, hash) != SHA_DIGEST_LENGTH)
    err(EXIT_FAILURE, "There was an error while generating the hash.");

    printf("SHA1 hash successfully generated.\n");

    /* ---------- Writing the hash ---------- */

    printf("Writing the hash to \"%s\".\n", output_name);

    FILE *output = fopen(output_name, "wb");

    if (!output)
    err(EXIT_FAILURE, "Could not open \"%s\" for writing.", output_name);

    if (fwrite(hash, sizeof(unsigned char), SHA_DIGEST_LENGTH, output)
    < SHA_DIGEST_LENGTH)
    err(EXIT_FAILURE, "There was an error while writing the hash.");

    printf("SHA1 successfully written.\n");

    /* ---------- Writing the weights ---------- */

    printf("Writing weights to \"%s\".\n", output_name);

    if (fwrite(weights, sizeof(double), WEIGHT_SIZE, output)
    < (size_t)WEIGHT_SIZE)
    err(EXIT_FAILURE, "There was an error while writing the weights.");

    printf("Weights successfully written.\n");

    /* ---------- Encrypting the file ---------- */

    /* ----- Reading ----- */

    printf("Reading \"%s\".\n", input_name);

    struct stat istics;

    if (stat(input_name, &istics))
    err(EXIT_FAILURE, "Could not retrieve informations on file.");

    FILE *input = fopen(input_name, "rb");

    if (!input)
    err(EXIT_FAILURE, "Could not open \"%s\".", input_name);

    char *c;
    c = malloc(istics.st_size);

    if (fread(c, 1, istics.st_size, input) < (unsigned long)istics.st_size)
    err(EXIT_FAILURE, "There was an error while reading the file.");

    printf("\"%s\" successfully read.\n", input_name);

    /* ----- Encrypting ----- */

    int value = make_value(weights, 10, WEIGHT_SIZE);
    char key[10];
    if (!make_key(weights, 10, key))
    err(EXIT_FAILURE, "Error generating key.");

    printf("Encrypting \"%s\" with %i.\n", input_name, value);

    for (size_t i = 0; i < (unsigned long)istics.st_size; i++)
    {
        c[i] = rot_echar(xor_char(c[i], key, i), value);
    }

    printf("\"%s\" successfully encrypted.\n", input_name);

    /* ----- Writing ----- */

    printf("Writing to \"%s\".\n", output_name);

    if (fwrite(c, 1, istics.st_size, output) < (unsigned long)istics.st_size)
    err(EXIT_FAILURE, "There was an error while writing the file.");

    free(c);

    printf("Writing was successful.\n");

    /* ---------- Closing ---------- */

    printf("Your file has successfully been encrypted.\n\
    The output was written in \"%s\".\n", output_name);

    fclose(input);
    remove(input_name);

    fclose(output);

    return;
}

void decrypt(char* input_name, char* pass)
{
    printf("\n");

    char output_name[1024];
    strcpy(output_name, input_name);
    output_name[strlen(input_name) - 4] = 0;

    /* ---------- Checking extension ---------- */

    const char *ext = extension(input_name);
    if (!strcmp(ext, "") || strcmp(ext, "vox"))
    {
        errx(EXIT_FAILURE, "\"%s\" is not a .vox file.", input_name);
    }

    /* ---------- Opening file ---------- */

    printf("Opening \"%s\" for reading.\n", input_name);

    FILE *input = fopen(input_name, "rb");

    if (!input)
    err(EXIT_FAILURE, "Could not open \"%s\".", input_name);

    printf("\"%s\" successfully opened.\n", input_name);

    /* ---------- Reading hash ---------- */

    printf("Reading hash.\n");

    unsigned char hash[SHA_DIGEST_LENGTH];

    if (fread(hash, sizeof(unsigned char), SHA_DIGEST_LENGTH, input)
    < SHA_DIGEST_LENGTH)
    err(EXIT_FAILURE, "There was an error reading the hash.");

    printf("Hash sucessfully read.\n");

    for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++)
    {
        printf("%x", hash[i]);
    }

    printf("\n");

    /* ---------- Reading weights ---------- */

    printf("Reading weights.\n");

    /* Read after the hash */
    if (fseek(input, sizeof(hash), SEEK_SET))
    err(EXIT_FAILURE, "There was an error moving the index.");

    double weights[WEIGHT_SIZE];

    if (fread(weights, sizeof(double), WEIGHT_SIZE, input) < WEIGHT_SIZE)
    err(EXIT_FAILURE, "There was an error reading the weights.");

    printf("Weights successfully read.\n");

    /* ---------- Checking the hash ---------- */

    printf("Checking hash.\n");

    unsigned char new_hash[SHA_DIGEST_LENGTH];

    if (sha1_double(weights, new_hash) != SHA_DIGEST_LENGTH)
    err(EXIT_FAILURE, "There was an error while generating the hash.");

    if (!check_sha1(hash, new_hash))
    errx(EXIT_FAILURE, "The weights do not match the hash. \
    The file must be corrupted.\
    V.O.X. cannot proceed.");

    printf("Hashes match.\n");

    /* ---------- Checking weights ---------- */

    printf("Checking audio passphrase.\n");

    if (!full_network(pass, weights))
    errx(EXIT_FAILURE, "The audio passphrase is wrong.");

    printf("Audio passphrase is correct.\n");

    /* ---------- Decrypting the file ---------- */
    /* ----- Reading ----- */

    printf("Reading the encrypted file.\n");

    /* Read after hash and weights */
    if (fseek(input, sizeof(hash) + sizeof(weights), SEEK_SET))
    err(EXIT_FAILURE, "There was an error moving the index.");

    struct stat istics;

    if (stat(input_name, &istics))
    err(EXIT_FAILURE, "Could not retrieve informations on file.");

    printf("Total size: %lu.\n", (size_t)(istics.st_size));
    printf("SHA size: %lu.\n", sizeof(hash));
    printf("Weights size: %lu.\n", sizeof(weights));

    size_t filesize = (size_t)istics.st_size - sizeof(weights) -sizeof(hash);

    printf("New size: %lu\n", filesize);

    char *c;
    c = malloc(filesize);

    if (fread(c, 1, filesize, input) < filesize)
    err(EXIT_FAILURE, "There was an error while reading the file.");

    printf("Encrypted file successfully read.\n");

    /* ----- Decrypting ----- */

    int value = make_value(weights, 10, WEIGHT_SIZE);
    char key[10];
    if (!make_key(weights, 10, key))
    err(EXIT_FAILURE, "Error generating key.");

    printf("Decrypting with %i.\n", value);

    for (size_t i = 0; i < filesize; i++)
    {
        c[i] = xor_char(rot_dchar(c[i], value), key, i);
    }

    printf("Decryption successful.\n");

    /* ----- Writing ----- */

    printf("Writing to \"%s\".\n", output_name);

    FILE *output = fopen(output_name, "wb");

    if(!output)
    err(EXIT_FAILURE, "Could not open \"%s\".", output_name);

    if (fwrite(c, 1, filesize, output) < filesize)
    err(EXIT_FAILURE, "There was an error while writing the file.");

    free(c);

    printf("Writing successful.\n");

    /* ---------- Closing ---------- */

    printf("Your file has successfully been decrypted.\n\
    The output was written in %s.\n", output_name);

    fclose(input);
    fclose(output);

    return;
}

int main(int argc, char* argv[])
{
    if (!strcmp(argv[1],"-e"))
    {
        encrypt(argv[2], argv[3], argv[4]);
        return 0;
    }
    else if (!strcmp(argv[1],"-d"))
    {
        decrypt(argv[2], argv[3]);
        return 0;
    }
    else
    {
        printf("Unknown command %s\n", argv[1]);
        return 1;
    }
}
