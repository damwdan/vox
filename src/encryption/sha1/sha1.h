/*
* SHA1 hash.
* Project: V.O.X.
* Developer: TheSandmen
*/

#include <openssl/sha.h>
#include <openssl/evp.h>

/* Writes SHA1 of input_file in hash */
void sha1(char* input_file, unsigned char* hash);
int sha1_double(double* tab, unsigned char* hash);

/* Checks if two files are the same by comparing SHA1 */
int check_sha1(unsigned char* file1, unsigned char* file2);

void print_hash(unsigned char* hash);
