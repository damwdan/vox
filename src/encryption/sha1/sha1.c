/*
* SHA1 hash
* Project: V.O.X.
* Developer: TheSandmen
*/

#include "sha1.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <err.h>

void sha1(char* input_file, unsigned char* hash)
{
    struct stat istics;

    if (stat(input_file, &istics))
    err(EXIT_FAILURE, "Could not retrieve informations on file.");

    FILE *input = fopen(input_file, "r+b");

    if (!input)
    errx(EXIT_FAILURE, "Cannot open %s.", input_file);

    char* c;
    c = malloc(istics.st_size);

    if (fread(c, 1, istics.st_size, input) < (unsigned long)istics.st_size)
    err(EXIT_FAILURE, "Could not read file.");

    SHA1((unsigned char*)c, istics.st_size, hash);

    fclose(input);
    free(c);

    return;
}

int sha1_double(double* tab, unsigned char* hash)
{
    OpenSSL_add_all_algorithms();

    unsigned int md_len = -1;
    const EVP_MD *md = EVP_get_digestbyname("SHA1");
    if (NULL != md)
    {
        EVP_MD_CTX mdctx;
        EVP_MD_CTX_init(&mdctx);
        EVP_DigestInit_ex(&mdctx, md, NULL);
        EVP_DigestUpdate(&mdctx, tab, sizeof(tab));
        EVP_DigestFinal_ex(&mdctx, hash, &md_len);
        EVP_MD_CTX_cleanup(&mdctx);
    }

    return md_len;
}

void print_hash(unsigned char* hash)
{
    for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++)
    {
        printf("%x", hash[i]);
    }

    printf("\n");
}

int check_sha1(unsigned char* file1, unsigned char* file2)
{
    int res = 1;

    for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++)
    {
        if (file1[i] != file2[i])
        {
            res = 0;
            break;
        }
    }

    if (!res)
    {
        return 0;
    }

    return 1;
}

/*int main(int argc, char *argv[])
{
    if (argc < 2)
    errx(EXIT_FAILURE, "Missing arguments : file path.");

    OpenSSL_add_all_algorithms();

    if (!strcmp(argv[1], "-c"))
    {
        if (argc < 4)
        errx(EXIT_FAILURE, "Missing arguments : file paths.");

        unsigned char hash1[SHA_DIGEST_LENGTH], hash2[SHA_DIGEST_LENGTH];
        sha1(argv[2], hash1);
        sha1(argv[3], hash2);

        return check_sha1(hash1, hash2);
    }

    if (!strcmp(argv[1], "-d"))
    {
        const double A[5] = {0.95373, 0.38934, 0.3983363, -0.393, -1.63};
        unsigned char hash[SHA_DIGEST_LENGTH], hash2[SHA_DIGEST_LENGTH];

        if (sha1_double(A, hash, 5) && sha1_double(A, hash2, 5))
        {
            printf("Test for: ");
            for (size_t i = 0; i < 5; i++)
            printf("%1.15f ", A[i]);

            printf("\n");

            for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++)
            {
                printf("%x", hash[i]);
            }

            printf("\n");

            for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++)
            {
                printf("%x", hash[i]);
            }

            printf("\n");

            return 0;
        }

        return 1;

    }


    if (!strncmp(argv[1], "-", 1))
    errx(EXIT_FAILURE, "Unknown parameter %s.", argv[1]);

    unsigned char hash[SHA_DIGEST_LENGTH];
    sha1(argv[1], hash);

    for (size_t i = 0; i < SHA_DIGEST_LENGTH; i++)
    {
        printf("%x", hash[i]);
    }

    printf("\n");

    return 0;

}*/
