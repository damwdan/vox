/*
* XOROT Encryption.
* Project: V.O.X.
* Developer: TheSandmen
*/

#include "xor/xor.h"
#include "rot/rot.h"
#include "sha1/sha1.h"
#include "../mlp/neurons.h"

value encrypt(value ocaml_input_name, value ocaml_pass, value ocaml_fake);

value decrypt(value ocaml_input_name, value ocaml_pass);
