/*
* XOROT Encryption.
* Project: V.O.X.
* Developer: TheSandmen
*/

#include "xor/xor.h"
#include "rot/rot.h"

/* README
* The XOROT encryption will apply a xor encryption followed by a rot encryption
* Run encrypt -t <message> <key> <value> to print a XOR encryption test.
* Run encrypt -e <file> <key> <value> to encrypt the file.
* Run encrypt -d <file> <key> <value> to decrypt the file.
*/

/*
* Applies XOROT encryption to the file input_name using key and value.
* encrypt will rename the original file with the .bak extension.
* encrypt will write the encrypted file with the .vox extension.
*/
void encrypt(char* input_name, char* pass, char* fake);

/*
* Applies XOROT decryption to the file input_name using key and value.
* decrypt will not remove the encrypted file.
* decrypt will write the decrypted file with the original name.
*/
void decrypt(char* input_name, char* pass);

/*
* Prints a XOROT test.
*/
void test(char* message, char* key, size_t value);
