/*
 * Play sound.
 * Project: V.O.X.
 * Developer: TheSandmen
 */

#define _XOPEN_SOURCE 500

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../inc/fmod.h"
#include "../inc/fmod_errors.h"
#include "../inc/wincompat.h"

#include <caml/alloc.h>
#include <caml/mlvalues.h>
#include <caml/memory.h>

#include "player.h"
#include "record.h"

void play(value ocaml_path)
{
  char* filepath = String_val(ocaml_path);
  if (!strcmp("recorded", filepath))
    filepath = "sounds/temp.wav";

  FMOD_SYSTEM *system;
  FMOD_SOUND *sound;
  FMOD_CHANNEL *channel = 0;
  FMOD_RESULT result;
  int key;

  result = FMOD_System_Create(&system);
  ERRCHECK(result);

  result = FMOD_System_Init(system, 32, FMOD_INIT_NORMAL, NULL);
  ERRCHECK(result);

  /* Opens sound file */
  result = FMOD_System_CreateSound(
                                   system
                                   , filepath
                                   , FMOD_SOFTWARE
                                   , 0
                                   , &sound);

  ERRCHECK(result);

  printf("==============================================================\n");
  printf("V.O.X. : Playing mode.\n");
  printf("==============================================================\n");

  result = FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, sound, 0, &channel);
  int aux = 0;

  do
    {
      unsigned int ms = 0;
      unsigned int lenms = 0;
      int playing = 0;

      if (channel)
        {
          FMOD_SOUND *currentsound = 0;

          result = FMOD_Channel_IsPlaying(channel, &playing);
          ERRCHECK(result);

          result = FMOD_Channel_GetPosition(channel, &ms, FMOD_TIMEUNIT_MS);
          ERRCHECK(result);

          FMOD_Channel_GetCurrentSound(channel, &currentsound);
          if (currentsound)
            {
              result = FMOD_Sound_GetLength(currentsound, &lenms, FMOD_TIMEUNIT_MS);
              ERRCHECK(result);
            }
        }

      result = FMOD_Sound_GetLength(sound, &lenms, FMOD_TIMEUNIT_MS);
      ERRCHECK(result);

      printf("Time %02d:%02d:%02d/%02d:%02d:%02d - %s\r",
             ms / 1000 / 60, ms / 1000 % 60, ms / 10 % 100,
             lenms / 1000 / 60, lenms / 1000 % 60, lenms / 10 % 100,
             playing ? "Playing" : "Stopped");

      aux = playing;

      fflush(stdout);
     
      Sleep(10);

    } while (aux);

  result = FMOD_Sound_Release(sound);
  ERRCHECK(result);
  result = FMOD_System_Close(system);
  ERRCHECK(result);
  result = FMOD_System_Release(system);
  ERRCHECK(result);
}
