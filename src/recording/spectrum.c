#include "record.h"
#include <caml/mlvalues.h>

#define WIDTH 768
#define HEIGHT 256
#define RATIO (HEIGHT / 255.0)
#define DELAY 25 /* 25 ms minimal value */
#define SPECTRUM_SIZE 512

void setPixel(SDL_Surface *surface, int x, int y, Uint32 pixel);

void recordSound(value ocaml_mode){
  int mode = Int_val(ocaml_mode);
  SDL_Surface *screen = NULL;
  SDL_Event event;
  SDL_Event previousEvent;
  int _continue = 1, startTime = 0, waitTime = 6000;
  TTF_Font *font;
  SDL_Rect position;
  SDL_Color textColor = {255, 255, 255};

  /* SDL init */
  SDL_putenv ("SDL_VIDEO_CENTERED=center");
  SDL_Init(SDL_INIT_VIDEO);
  screen = SDL_SetVideoMode(WIDTH, HEIGHT*2, 32, SDL_SWSURFACE | SDL_DOUBLEBUF);
  SDL_WM_SetCaption("Sound recording", NULL);

  /* TTF_init */
  if(TTF_Init() == -1){
    fprintf(stderr, "TTF_Init error: %s\n", TTF_GetError());
    exit(EXIT_FAILURE);
  }
  font = TTF_OpenFont("recording/luxisr.ttf", 16);

  // 5 sec wait time
  startTime = SDL_GetTicks();
  while ((SDL_GetTicks() - startTime <= waitTime) && _continue){
    // events
    while(SDL_PollEvent(&event)){
      switch(event.type){
      case SDL_QUIT:
        _continue  = 0;
        break;
      case SDL_KEYDOWN:
        switch(event.key.keysym.sym){
        case SDLK_ESCAPE:
          _continue  = 0;
          break;
        }
      }
    }
    SDL_Surface *text;
    char t[128];
    SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
    int delta = (waitTime - (SDL_GetTicks() - startTime))/1000;
    if (delta < 0)
      delta = 0;
    sprintf(t, "Recording in: %d second(s)", delta);
    text = TTF_RenderText_Blended(font, t, textColor);
    position.x = WIDTH/2 - (text->w)/2;
    position.y = HEIGHT - (text->h);
    SDL_BlitSurface(text, NULL, screen, &position);
    SDL_Flip(screen);
    SDL_FreeSurface(text);
  }
  // recording
  if(_continue){
    SDL_Surface *text2;
    sample(screen, font, text2, mode);
    _continue = 1;
    while(_continue){
      // events
      while(SDL_PollEvent(&event)){
        switch(event.type){
        case SDL_QUIT:
          _continue  = 0;
          break;
        case SDL_KEYDOWN:
          switch(event.key.keysym.sym){
          case SDLK_ESCAPE:
            _continue  = 0;
            break;
          }
        }
      }
      SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
      SDL_Surface *text;
      text = TTF_RenderText_Blended(font, "Recording done! Press Escape to continue.", textColor);
      position.x = WIDTH/2 - (text->w)/2;
      position.y = HEIGHT - (text->h);
      SDL_BlitSurface(text, NULL, screen, &position);
      SDL_Flip(screen);
      SDL_FreeSurface(text);
    }
  }
  TTF_CloseFont(font);
  TTF_Quit();
  SDL_FreeSurface(screen);
  SDL_Quit();
}

// --------------------------------- //
//     Play sound with preview
// --------------------------------- //
int previewSound(value ocaml_path)
{
  char *file = String_val(ocaml_path);
  SDL_Surface *screen = NULL;
  SDL_Event event;
  SDL_Event previousEvent;
  int _continue = 1, lineHeight = 0, currentTime = 0;
  int spectrumMode = 0;
  int noise_active = 0;
  float noise[SPECTRUM_SIZE];
  for (int i = 0; i < SPECTRUM_SIZE; i++)
    noise[i] = 0;
  float spectrum_left[SPECTRUM_SIZE];
  float spectrum_right[SPECTRUM_SIZE];
  float s_left[HEIGHT*2];
  float s_right[HEIGHT*2];
  float samples_l[120][HEIGHT*2];
  float samples_r[120][HEIGHT*2];
  for(int i = 0; i < 120; i++){
    for(int j = 0; j < HEIGHT*2; j++){
      samples_l[i][j] = 0;
      samples_r[i][j] = 0;
    }
  }
  float logo_spectrum_left[64];
  float logo_spectrum_right[64];
  FMOD_SYSTEM *system;
  FMOD_SOUND *password;
  FMOD_CHANNEL *channel;
  FMOD_RESULT result;
  TTF_Font *font = NULL;
  SDL_Surface *text, *text2, *text3, *text4 = NULL, *text5, *text6, *text7;
  SDL_Color textColor = {255, 255, 255};
  SDL_Rect position;
  SDL_Surface *logo;
  logo = SDL_LoadBMP("recording/logo_white.bmp");

  // FMOD init
  FMOD_System_Create(&system);
  FMOD_System_Init(system, 1, FMOD_INIT_NORMAL, NULL);
  result = FMOD_System_CreateSound(system, file, FMOD_SOFTWARE | FMOD_2D | FMOD_CREATESTREAM, 0, &password);
  if (result != FMOD_OK){
    fprintf(stderr, "File importation error.\n");
    exit(EXIT_FAILURE);
  }
  FMOD_System_GetChannel(system, 0, &channel); // getting channel pointer
  // SDL init
  SDL_putenv ("SDL_VIDEO_CENTERED=center");
  SDL_Init(SDL_INIT_VIDEO);
  screen = SDL_SetVideoMode(WIDTH, HEIGHT*2, 32, SDL_SWSURFACE | SDL_DOUBLEBUF);
  SDL_WM_SetCaption("Sound preview", NULL);
  // TTF_init
  if(TTF_Init() == -1){
    fprintf(stderr, "TTF_Init error: %s\n", TTF_GetError());
    exit(EXIT_FAILURE);
  }
  font = TTF_OpenFont("recording/luxisr.ttf", 16);
  text = TTF_RenderText_Blended(font, "Left channel", textColor);
  text2 = TTF_RenderText_Blended(font, "Right channel", textColor);
  text3 = TTF_RenderText_Blended(font, "Press Escape to quit", textColor);
  text4 = TTF_RenderText_Blended(font, "Press Space to pause/play the sound", textColor);
  text5 = TTF_RenderText_Blended(font, "Press Enter for noise reduction (ON)", textColor);
  text7 = TTF_RenderText_Blended(font, "Press Enter for noise reduction (OFF)", textColor);
  text6 = TTF_RenderText_Blended(font, "Press M to change display mode", textColor);

  // main loop
  FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, password, 0, NULL);
  while (_continue){
    // keyboard events
    while(SDL_PollEvent(&event)){
      switch(event.type){
      case SDL_QUIT:
        _continue  = 0;
        break;
      case SDL_KEYDOWN:
        switch(event.key.keysym.sym){
        case SDLK_SPACE:
          {
            FMOD_BOOL isPlaying;
            FMOD_Channel_IsPlaying(channel, &isPlaying);
            if(isPlaying){
              FMOD_BOOL state;
              FMOD_Channel_GetPaused(channel, &state);
              if (state)
                FMOD_Channel_SetPaused(channel, 0);
              else
                FMOD_Channel_SetPaused(channel, 1);
            }
            else{
              FMOD_System_PlaySound(system, FMOD_CHANNEL_FREE, password, 0, NULL);
            }
          }
          break;
        case SDLK_ESCAPE:
          _continue  = 0;
          break;
        case SDLK_RETURN:
          noise_active  = !noise_active;
          break;
	case SDLK_m:
	  spectrumMode  = !spectrumMode;
          break;
        }
      }
    }
    int precCorrected = 1;
    // basic real-time spectrogram
    if (!spectrumMode){
      SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
      FMOD_BOOL isPaused;
      FMOD_Channel_GetPaused(channel, &isPaused);
      if (!isPaused){
	int playpos;
	FMOD_Channel_GetPosition(channel, &playpos, FMOD_TIMEUNIT_MS);
	if (playpos < NOISE_DELAY){
	  float temp[SPECTRUM_SIZE];
	  FMOD_Channel_GetSpectrum(channel, temp, SPECTRUM_SIZE, 0,  FMOD_DSP_FFT_WINDOW_RECT);
	  for (int i = 0; i < SPECTRUM_SIZE; i++) {
	    if (noise[i] < temp[i])
	      noise[i] = temp[i];
	  }
	}
      }
      FMOD_Channel_GetSpectrum(channel, spectrum_left, SPECTRUM_SIZE, 0,  FMOD_DSP_FFT_WINDOW_RECT);
      FMOD_Channel_GetSpectrum(channel, spectrum_right, SPECTRUM_SIZE, 1,  FMOD_DSP_FFT_WINDOW_RECT);
      SDL_LockSurface(screen);
      int offset = (WIDTH - SPECTRUM_SIZE)/2;
      // green lines
      // horizontal
      int grid = 11; 
      while(grid < HEIGHT * 2){
	for (int l = 0; l < WIDTH; l++){
	  *((unsigned int *)screen->pixels + WIDTH * grid + l) = SDL_MapRGB(screen->format, 0, 42, 0);
	}
	grid += 35;
      }
      // vertical
      grid = 28; 
      while(grid < WIDTH){
	for (int l = 0; l < HEIGHT * 2; l++){
	  *((unsigned int *)screen->pixels + WIDTH * l + grid) = SDL_MapRGB(screen->format, 0, 42, 0);
	}
	grid += 50;
      }
      // graduations
      float interval = 11;
      while(interval < HEIGHT*2){
	for (int l = offset - 4; l < offset + 4; l++){
	  *((unsigned int *)screen->pixels + WIDTH * (int)interval + l) = SDL_MapRGB(screen->format, 0, 100, 0);
	}
	interval += 17.5;
      }
      interval = 3;
      while(interval < WIDTH){
	for (int l = HEIGHT - 4; l < HEIGHT + 4; l++){
	  *((unsigned int *)screen->pixels + WIDTH * l + (int)interval) = SDL_MapRGB(screen->format, 0, 100, 0);
	}
	interval += 25;
      }
      // horizontal line
      for (int l = 0; l < WIDTH; l++){
	*((unsigned int *)screen->pixels + WIDTH * HEIGHT + l) = SDL_MapRGB(screen->format, 0, 255, 0);
      }
      // vertical line
      for (int l = 0; l < HEIGHT * 2; l++){
	*((unsigned int *)screen->pixels + WIDTH * l + offset) = SDL_MapRGB(screen->format, 0, 255, 0);
      }
      // spectrum
      for (int i = 0; i < SPECTRUM_SIZE; i++){
	// left
	if (noise_active)
	  lineHeight = (noise_band(spectrum_left[i], noise[i], &precCorrected)) * 20 * HEIGHT;
	else
	  lineHeight = spectrum_left[i] * 20 * HEIGHT;

	if (lineHeight > HEIGHT)
	  lineHeight = HEIGHT;

	for (int j = HEIGHT - lineHeight ; j < HEIGHT ; j++){
	  setPixel(screen, i+offset, j, SDL_MapRGB(screen->format, 255 - (j / RATIO), j / RATIO, 0));
	}
	// right
	if (noise_active)
	  lineHeight = (noise_band(spectrum_right[i], noise[i], &precCorrected)) * 20 * HEIGHT;
	else
	  lineHeight = spectrum_right[i] * 20 * HEIGHT;

	if (lineHeight > HEIGHT)
	  lineHeight = HEIGHT;

	for (int k = HEIGHT; k < HEIGHT + lineHeight; k++){
	  setPixel(screen, i+offset, k, SDL_MapRGB(screen->format, (k - HEIGHT) / RATIO, 255 - ((k-HEIGHT) / RATIO), 0));
	}
      }
      SDL_UnlockSurface(screen);
    } else { // alternate spectrogram
      SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
      FMOD_Channel_GetSpectrum(channel, s_left, HEIGHT * 2, 0,  FMOD_DSP_FFT_WINDOW_RECT);
      FMOD_Channel_GetSpectrum(channel, s_right, HEIGHT * 2, 1,  FMOD_DSP_FFT_WINDOW_RECT);
      SDL_LockSurface(screen);
      int offset = 2;
      int beginPos = 178;
      int playpos;
      FMOD_Channel_GetPosition(channel, &playpos, FMOD_TIMEUNIT_MS);
      playpos /= 25;
      if(!(playpos >= 0 && playpos < 120))
	playpos = 119;	
      for(int i = 0; i < HEIGHT; i++){
	if (noise_active) {
	  samples_l[playpos][i] = (noise_band(s_left[i], noise[i], &precCorrected));
	  samples_r[playpos][i] = (noise_band(s_right[i], noise[i], &precCorrected));
	}
	else{
	  samples_l[playpos][i] = s_left[i];
	  samples_r[playpos][i] = s_right[i];
	}
      }
      // spectrogram
      for(int i = 0; i < playpos; i++){
	for(int k = 0; k < offset; k++){
	  for (int j = 0 ; j < HEIGHT ; j++){
	    float sl = samples_l[i][j] * 200;
	    float sr = samples_r[i][j] * 200;
	    // left
	    setPixel(screen, beginPos + i*offset + k, HEIGHT - j, SDL_MapRGB(screen->format, 255 * sl, 0, 0));
	    // right
	    setPixel(screen, beginPos + i*offset + k, HEIGHT + j, SDL_MapRGB(screen->format, 255 * sr, 0, 0));
	  }
	}
      }
      // red lines
      int grid = 11; 
      while(grid < HEIGHT * 2){
	for (int l = 0; l < WIDTH; l++){
	  *((unsigned int *)screen->pixels + WIDTH * grid + l) = SDL_MapRGB(screen->format, 42, 0, 0);
	}
	grid += 35;
      }
      grid = 28; 
      while(grid < WIDTH){
	for (int l = 0; l < HEIGHT * 2; l++){
	  *((unsigned int *)screen->pixels + WIDTH * l + grid) = SDL_MapRGB(screen->format, 42, 0, 0);
	}
	grid += 50;
      }
      // graduations
      float interval = 11;
      while(interval < HEIGHT*2){
	for (int l = beginPos - 4; l < beginPos + 4; l++){
	  *((unsigned int *)screen->pixels + WIDTH * (int)interval + l) = SDL_MapRGB(screen->format, 100, 0, 0);
	}
	interval += 17.5;
      }
      interval = 3;
      while(interval < WIDTH){
	for (int l = HEIGHT - 4; l < HEIGHT + 4; l++){
	  *((unsigned int *)screen->pixels + WIDTH * l + (int)interval) = SDL_MapRGB(screen->format, 100, 0, 0);
	}
	interval += 25;
      }
      // horizontal line
      for (int l = 0; l < WIDTH; l++){
	*((unsigned int *)screen->pixels + WIDTH * HEIGHT + l) = SDL_MapRGB(screen->format, 255, 0, 0);
      }
      // vertical line
      for (int l = 0; l < HEIGHT * 2; l++){
	*((unsigned int *)screen->pixels + WIDTH * l + beginPos) = SDL_MapRGB(screen->format, 255, 0, 0);
      }
      SDL_UnlockSurface(screen);
    }
    SDL_LockSurface(screen);
    // black rectangles
    // left channel
    for(int i = 0; i < 115; i++){
      for (int y = 82; y < 116; y++){
	*((unsigned int *)screen->pixels + WIDTH * y + i) = SDL_MapRGB(screen->format, 0, 0, 0);
      }
    }
    // right channel
    for(int i = 0; i < 115; i++){
      for (int y = 397; y < 431; y++){
	*((unsigned int *)screen->pixels + WIDTH * y + i) = SDL_MapRGB(screen->format, 0, 0, 0);
      }
    }
    // commands and logo
    for(int i = 479; i < 768; i++){
      for (int y = 47; y < 186; y++){
	*((unsigned int *)screen->pixels + WIDTH * y + i) = SDL_MapRGB(screen->format, 0, 0, 0);
      }
    }
    for(int i = 479; i < 768; i++){
      for (int y = 397; y < 466; y++){
	*((unsigned int *)screen->pixels + WIDTH * y + i) = SDL_MapRGB(screen->format, 0, 0, 0);
      }
    }
    // logo
    SDL_UnlockSurface(screen);
    position.x = 565;
    position.y = 411;
    SDL_BlitSurface(logo, NULL, screen, &position); 
    SDL_LockSurface(screen);	
    FMOD_Channel_GetSpectrum(channel, logo_spectrum_left, 64, 0,  FMOD_DSP_FFT_WINDOW_RECT);
    FMOD_Channel_GetSpectrum(channel, logo_spectrum_right, 64, 1,  FMOD_DSP_FFT_WINDOW_RECT);
    int number = 4 * 70;
    drawSquares(screen, 550, 443, constrain((int)number * logo_spectrum_left[0], 0, 4), 8, 10);
    drawSquares(screen, 540, 443, constrain((int)number * logo_spectrum_left[1], 0, 4), 8, 10);
    drawSquares(screen, 530, 443, constrain((int)number * logo_spectrum_left[2], 0, 4), 8, 10);
    drawSquares(screen, 520, 443, constrain((int)number * logo_spectrum_left[3], 0, 4), 8, 10);
    drawSquares(screen, 725, 443, constrain((int)number * logo_spectrum_right[3], 0, 4), 8, 10);
    drawSquares(screen, 715, 443, constrain((int)number * logo_spectrum_right[2], 0, 4), 8, 10);
    drawSquares(screen, 705, 443, constrain((int)number * logo_spectrum_right[1], 0, 4), 8, 10);
    drawSquares(screen, 695, 443, constrain((int)number * logo_spectrum_right[0], 0, 4), 8, 10);
    SDL_UnlockSurface(screen);
    // text
    // left channel
    position.x = 20;
    position.y = 90;
    SDL_BlitSurface(text, NULL, screen, &position);
    // right channel
    position.x = 15;
    position.y = HEIGHT*2 - 88 - text2->h;
    SDL_BlitSurface(text2, NULL, screen, &position);
    // press escape
    position.x = WIDTH - text4->w - 10;
    position.y = 60;
    SDL_BlitSurface(text3, NULL, screen, &position);
    // press space
    position.x = WIDTH - text4->w - 10;
    position.y = 90;
    SDL_BlitSurface(text4, NULL, screen, &position);  
    //press m
    position.x = WIDTH - text4->w - 10;
    position.y = 120;
    SDL_BlitSurface(text6, NULL, screen, &position);
    // noise reduction
    position.x = WIDTH - text4->w - 10;
    position.y = 150;
    if (noise_active)
      SDL_BlitSurface(text5, NULL, screen, &position);
    else
      SDL_BlitSurface(text7, NULL, screen, &position);
    SDL_Flip(screen);
  }
  // close properly
  FMOD_Sound_Release(password);
  FMOD_System_Close(system);
  FMOD_System_Release(system);
  TTF_CloseFont(font);
  TTF_Quit();
  SDL_FreeSurface(text);
  SDL_FreeSurface(text2);
  SDL_FreeSurface(text3);
  SDL_FreeSurface(text4);
  SDL_FreeSurface(screen);
  SDL_FreeSurface(logo);
  SDL_Quit();
  return EXIT_SUCCESS;
}
