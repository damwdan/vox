/*
 * Record to disk.
 * Project: V.O.X.
 * Developer: TheSandmen
 */

#include "record.h"

// auxiliary functions

void drawSquare(SDL_Surface *screen, int x, int y, int size){
  for(int i = x; i < x + size; i++){
    for (int j = y; j < y + size; j++){
      *((unsigned int *)screen->pixels + WIDTH * j + i) = SDL_MapRGB(screen->format, 255, 255, 255);
    }
  }
}

void drawSquares(SDL_Surface *screen, int x, int y, int number, int size, int offset){
  for(int i = 0; i < number; i++){
    drawSquare(screen, x, y - i*offset, size);
  }
}

int constrain(int x, int min, int max){
  if(x < min)
    return min;
  else if(x > max)
    return max;
  else
    return x;
}

float noise_band(float spectrum, float noise, int *prec){
  float res = spectrum - noise;
  
  *prec = (spectrum <= noise * 1.15);
  if (*prec)
    {
      if (res <= 0)
        return 0;
      else
        return res;
    }
  else
    return spectrum;
}

void ERRCHECK(FMOD_RESULT result)
{
  if (result != FMOD_OK)
    {
      printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
      exit(-1);
    }
}

#if defined(WIN32) || defined(__WATCOMC__) || defined(_WIN32) || defined(__WIN32__)
#define __PACKED                         /* dummy */
#else
#define __PACKED __attribute__((packed)) /* gcc packed */
#endif

void SaveToWav(FMOD_SOUND *sound, int mode)
{
  FILE *fp;
  int             channels, bits;
  float           rate;
  void           *ptr1, *ptr2;
  unsigned int    lenbytes, len1, len2;

  if (!sound)
    {
      return;
    }

  FMOD_Sound_GetFormat  (sound, 0, 0, &channels, &bits);
  FMOD_Sound_GetDefaults(sound, &rate, 0, 0, 0);
  FMOD_Sound_GetLength  (sound, &lenbytes, FMOD_TIMEUNIT_PCMBYTES);

  {
#if defined(WIN32) || defined(_WIN64) || defined(__WATCOMC__) || defined(_WIN32) || defined(__WIN32__)
#pragma pack(1)
#endif
        
    /*
      WAV Structures
    */
    typedef struct
    {
      signed char id[4];
      int 		size;
    } RiffChunk;
    
    struct
    {
      RiffChunk       chunk           __PACKED;
      unsigned short	wFormatTag      __PACKED;    /* format type  */
      unsigned short	nChannels       __PACKED;    /* number of channels (i.e. mono, stereo...)  */
      unsigned int	nSamplesPerSec  __PACKED;    /* sample rate  */
      unsigned int	nAvgBytesPerSec __PACKED;    /* for buffer estimation  */
      unsigned short	nBlockAlign     __PACKED;    /* block size of data  */
      unsigned short	wBitsPerSample  __PACKED;    /* number of bits per sample of mono data */
    } __PACKED FmtChunk  = { {{'f','m','t',' '}, sizeof(FmtChunk) - sizeof(RiffChunk) }, 1, channels, (int)rate, (int)rate * channels * bits / 8, 1 * channels * bits / 8, bits };

    struct
    {
      RiffChunk   chunk;
    } DataChunk = { {{'d','a','t','a'}, lenbytes } };

    struct
    {
      RiffChunk   chunk;
      signed char rifftype[4];
    } WavHeader = { {{'R','I','F','F'}, sizeof(FmtChunk) + sizeof(RiffChunk) + lenbytes }, {'W','A','V','E'} };

#if defined(WIN32) || defined(_WIN64) || defined(__WATCOMC__) || defined(_WIN32) || defined(__WIN32__)
#pragma pack()
#endif

    if (mode == 0)
      fp = fopen("sounds/temp_e.wav", "wb");
    else if (mode == 1)
      fp = fopen("sounds/temp_d.wav", "wb");
    else if (mode == 2)
      fp = fopen("sounds/temp_w.wav", "wb");

    /*
      Write out the WAV header.
    */
    fwrite(&WavHeader, sizeof(WavHeader), 1, fp);
    fwrite(&FmtChunk, sizeof(FmtChunk), 1, fp);
    fwrite(&DataChunk, sizeof(DataChunk), 1, fp);

    /*
      Lock the sound to get access to the raw data.
    */
    FMOD_Sound_Lock(sound, 0, lenbytes, &ptr1, &ptr2, &len1, &len2);

    /*
      Write it to disk.
    */
    fwrite(ptr1, len1, 1, fp);

    /*
      Unlock the sound to allow FMOD to use it again.
    */
    FMOD_Sound_Unlock(sound, ptr1, ptr2, len1, len2);

    fclose(fp);
  }
}


void sample(SDL_Surface *screen, TTF_Font *font, SDL_Surface *text, int mode)
{
  FMOD_SYSTEM           *system  = 0;
  FMOD_SOUND            *sound   = 0;
  FMOD_CHANNEL          *channel = 0;
  FMOD_RESULT            result;
  FMOD_CREATESOUNDEXINFO exinfo;
  int                    key, driver, recorddriver, numdrivers, count;
  unsigned int           version;
  float spectrum_left[SPECTRUM_SIZE];
  float spectrum_right[SPECTRUM_SIZE];
  int offset = (WIDTH - SPECTRUM_SIZE)/2, lineHeight = 0;
  SDL_Rect position;
  SDL_Color textColor = {255, 255, 255};
  int datalength = 0;
  int startTime;
  SDL_Surface *logo;
  logo = SDL_LoadBMP("recording/logo_white.bmp");
  float logo_spectrum_left[64];
  float logo_spectrum_right[64];

  /* Create a System object and initialize. */
  result = FMOD_System_Create(&system);
  ERRCHECK(result);

  result = FMOD_System_GetVersion(system, &version);
  ERRCHECK(result);

  if (version < FMOD_VERSION)
    {
      printf("Error!  You are using an old version of FMOD %08x.  This program requires %08x\n", version, FMOD_VERSION);
      return;
    }

  /* System initialization */
  result = FMOD_System_SetOutput(system, FMOD_OUTPUTTYPE_ALSA); 
  ERRCHECK(result);
    
  /* playback device */
  result = FMOD_System_GetNumDrivers(system, &numdrivers);
  ERRCHECK(result);
  char name[256];
  result = FMOD_System_GetDriverInfo(system, 0, name, 256, 0);
  ERRCHECK(result);
  driver = 0;
  result = FMOD_System_SetDriver(system, driver);
  ERRCHECK(result);

  /* record device */
  result = FMOD_System_GetRecordNumDrivers(system, &numdrivers);
  ERRCHECK(result);

  char name2[256];
  result = FMOD_System_GetRecordDriverInfo(system, 0, name2, 256, 0);
  ERRCHECK(result);
  recorddriver = 0;
  
  /* recording mode */
  result = FMOD_System_Init(system, 32, FMOD_INIT_NORMAL, NULL);
  ERRCHECK(result);
  memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));
  exinfo.cbsize           = sizeof(FMOD_CREATESOUNDEXINFO);
  exinfo.numchannels      = 2;
  exinfo.format           = FMOD_SOUND_FORMAT_PCM16;
  exinfo.defaultfrequency = 44100;
  exinfo.length           = exinfo.defaultfrequency * sizeof(short) * exinfo.numchannels * 3;
  result = FMOD_System_CreateSound(system, 0, FMOD_2D | FMOD_SOFTWARE | FMOD_OPENUSER, &exinfo, &sound);
  ERRCHECK(result);

  /* start record and playback */
  startTime = SDL_GetTicks();
  result = FMOD_System_RecordStart(system, recorddriver, sound, 0);
  ERRCHECK(result);
  Sleep(50);
  result = FMOD_System_PlaySound(system, FMOD_CHANNEL_REUSE, sound, 0, &channel);
  ERRCHECK(result);
  FMOD_Channel_SetVolume(channel,0.0f);//mute the channel
  /* main loop */
  do
    {
      static int   looping   = 0;
      int          recording = 0;
      int          playing   = 0;
      unsigned int recordpos = 0;
      unsigned int playpos   = 0;
      unsigned int length;

      FMOD_Sound_GetLength(sound, &length, FMOD_TIMEUNIT_PCM);
      ERRCHECK(result);
      FMOD_System_IsRecording(system, recorddriver, &recording);
      ERRCHECK(result);
      FMOD_System_GetRecordPosition(system, recorddriver, &recordpos);
      ERRCHECK(result);
      FMOD_Channel_GetPosition(channel, &playpos, FMOD_TIMEUNIT_PCM);
      ERRCHECK(result);
      FMOD_Sound_GetLength(sound, &datalength, FMOD_TIMEUNIT_PCMBYTES);
      ERRCHECK(result);

      FMOD_System_Update(system);
      if (SDL_GetTicks() - startTime > 1000)
        {
	  /* Drawing the spectrum */
	  FMOD_Channel_GetSpectrum(channel, spectrum_left, SPECTRUM_SIZE, 0,  FMOD_DSP_FFT_WINDOW_RECT);
	  FMOD_Channel_GetSpectrum(channel, spectrum_right, SPECTRUM_SIZE, 1,  FMOD_DSP_FFT_WINDOW_RECT);
	  SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0));
	  SDL_LockSurface(screen);

	  // green grid
	  int grid = 11; 
	  while(grid < HEIGHT * 2){
	    for (int l = 0; l < WIDTH; l++){
	      *((unsigned int *)screen->pixels + WIDTH * grid + l) = SDL_MapRGB(screen->format, 0, 42, 0);
	    }
	    grid += 35;
	  }
	  grid = 28; 
	  while(grid < WIDTH){
	    for (int l = 0; l < HEIGHT * 2; l++){
	      *((unsigned int *)screen->pixels + WIDTH * l + grid) = SDL_MapRGB(screen->format, 0, 42, 0);
	    }
	    grid += 50;
	  }

	  // graduations
	  float interval = 11;
	  while(interval < HEIGHT*2){
	    for (int l = offset - 4; l < offset + 4; l++){
	      *((unsigned int *)screen->pixels + WIDTH * (int)interval + l) = SDL_MapRGB(screen->format, 0, 100, 0);
	    }
	    interval += 17.5;
	  }
	  interval = 3;
	  while(interval < WIDTH){
	    for (int l = HEIGHT - 4; l < HEIGHT + 4; l++){
	      *((unsigned int *)screen->pixels + WIDTH * l + (int)interval) = SDL_MapRGB(screen->format, 0, 100, 0);
	    }
	    interval += 25;
	  }
	  // horizontal line
	  for (int l = 0; l < WIDTH; l++){
	    *((unsigned int *)screen->pixels + WIDTH * HEIGHT + l) = SDL_MapRGB(screen->format, 0, 255, 0);
	  }
	  // vertical line
	  for (int l = 0; l < HEIGHT * 2; l++){
	    *((unsigned int *)screen->pixels + WIDTH * l + offset) = SDL_MapRGB(screen->format, 0, 255, 0);
	  }

	  // spectrum
	  for (int i = 0; i < SPECTRUM_SIZE; i++){
	    /* Left */
	    lineHeight = spectrum_left[i] * 20 * HEIGHT;
	    if (lineHeight > HEIGHT)
	      lineHeight = HEIGHT;
	    if (lineHeight < 0)
	      lineHeight = 0;
	    for (int j = HEIGHT - lineHeight ; j < HEIGHT ; j++){
	      setPixel(screen, i+offset, j, SDL_MapRGB(screen->format, 255 - (j / RATIO), j / RATIO, 0));
	    }
	    /* Right */
	    lineHeight = spectrum_right[i] * 20 * HEIGHT;
	    if (lineHeight > HEIGHT)
	      lineHeight = HEIGHT;
	    if (lineHeight < 0)
	      lineHeight = 0;
	    for (int k = HEIGHT; k < HEIGHT + lineHeight; k++){		
	      setPixel(screen, i+offset, k, SDL_MapRGB(screen->format, (k - HEIGHT) / RATIO, 255 - ((k-HEIGHT) / RATIO), 0));
	    }
	  }

	  // black rectangles
	  // left channel
	  for(int i = 0; i < 115; i++){
	    for (int y = 82; y < 116; y++){
	      *((unsigned int *)screen->pixels + WIDTH * y + i) = SDL_MapRGB(screen->format, 0, 0, 0);
	    }
	  }
	  // logo
	  for(int i = 479; i < 768; i++){
	    for (int y = 397; y < 466; y++){
	      *((unsigned int *)screen->pixels + WIDTH * y + i) = SDL_MapRGB(screen->format, 0, 0, 0);
	    }
	  }
	  // record position
	  for(int i = 229; i < 728; i++){
	    for (int y = 47; y < 81; y++){
	      *((unsigned int *)screen->pixels + WIDTH * y + i) = SDL_MapRGB(screen->format, 0, 0, 0);
	    }
	  }
	  // right channel
	  for(int i = 0; i < 115; i++){
	    for (int y = 397; y < 431; y++){
	      *((unsigned int *)screen->pixels + WIDTH * y + i) = SDL_MapRGB(screen->format, 0, 0, 0);
	    }
	  }

	  // logo
	  SDL_UnlockSurface(screen);
	  position.x = 565;
	  position.y = 411;
	  SDL_BlitSurface(logo, NULL, screen, &position); 
	  SDL_LockSurface(screen);
	  FMOD_Channel_GetSpectrum(channel, logo_spectrum_left, 64, 0,  FMOD_DSP_FFT_WINDOW_RECT);
	  FMOD_Channel_GetSpectrum(channel, logo_spectrum_right, 64, 1,  FMOD_DSP_FFT_WINDOW_RECT);
	  int number = 4 * 70;
	  drawSquares(screen, 550, 443, constrain((int)number * logo_spectrum_left[0], 0, 4), 8, 10);
	  drawSquares(screen, 540, 443, constrain((int)number * logo_spectrum_left[1], 0, 4), 8, 10);
	  drawSquares(screen, 530, 443, constrain((int)number * logo_spectrum_left[2], 0, 4), 8, 10);
	  drawSquares(screen, 520, 443, constrain((int)number * logo_spectrum_left[3], 0, 4), 8, 10);
	  drawSquares(screen, 725, 443, constrain((int)number * logo_spectrum_right[3], 0, 4), 8, 10);
	  drawSquares(screen, 715, 443, constrain((int)number * logo_spectrum_right[2], 0, 4), 8, 10);
	  drawSquares(screen, 705, 443, constrain((int)number * logo_spectrum_right[1], 0, 4), 8, 10);
	  drawSquares(screen, 695, 443, constrain((int)number * logo_spectrum_right[0], 0, 4), 8, 10);
	  SDL_UnlockSurface(screen);

	  // texts
	  char t[128] = "";
	  sprintf(t, "Record: %6d  Play: %6d  Length: %6d", recordpos, playpos, datalength);
	  text = TTF_RenderText_Blended(font, t, textColor);
	  position.x = WIDTH/2 - 120;
	  position.y = HEIGHT/3 - (text->h) - 11;
	  SDL_BlitSurface(text, NULL, screen, &position);
	  // left channel
	  text = TTF_RenderText_Blended(font, "Left channel", textColor);
	  position.x = 20;
	  position.y = 90;
	  SDL_BlitSurface(text, NULL, screen, &position);
	  // right channel
	  text = TTF_RenderText_Blended(font, "Right channel", textColor);
	  position.x = 15;
	  position.y = HEIGHT*2 - 88 - text->h;
	  SDL_BlitSurface(text, NULL, screen, &position);

	  SDL_Flip(screen);
	  SDL_FreeSurface(text);
        }
      Sleep(10);
    } while (SDL_GetTicks() - startTime <= 4100);
    
  /* Writing sound */
  SaveToWav(sound, mode);
  Sleep(500);
  /* close */
  result = FMOD_Sound_Release(sound);
  ERRCHECK(result);
  result = FMOD_System_Release(system);
  ERRCHECK(result);
}

void setPixel(SDL_Surface *surface, int x, int y, Uint32 pixel){
  int bpp = surface->format->BytesPerPixel;
 
  Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
 
  switch(bpp) {
  case 1:
    *p = pixel;
    break;
 
  case 2:
    *(Uint16 *)p = pixel;
    break;
 
  case 3:
    if(SDL_BYTEORDER == SDL_BIG_ENDIAN) {
      p[0] = (pixel >> 16) & 0xff;
      p[1] = (pixel >> 8) & 0xff;
      p[2] = pixel & 0xff;
    } else {
      p[0] = pixel & 0xff;
      p[1] = (pixel >> 8) & 0xff;
      p[2] = (pixel >> 16) & 0xff;
    }
    break;
 
  case 4:
    *(Uint32 *)p = pixel;
    break;
  }
}

/* Writes sound to file */

void WriteWavHeader(FILE *file, FMOD_SOUND *sound, int length)
{
  int channels, bits;
  float rate;

  if (!sound)
    return;

  fseek(file, 0, SEEK_SET);

  FMOD_Sound_GetFormat  (sound, 0, 0, &channels, &bits);
  FMOD_Sound_GetDefaults(sound, &rate, 0, 0, 0);
  Wav FmtChunk  =
    {
      {{'f','m','t',' '}
       , sizeof(FmtChunk) - sizeof(RiffChunk) }
      , 1
      , channels
      , (int)rate
      , (int)rate * channels * bits / 8
      , 1 * channels * bits / 8
      , bits
    };

  struct {RiffChunk chunk;} DataChunk =
                              {
                                {{'d','a','t','a'}
                                 , length}
                              };

  struct {RiffChunk chunk; signed char rifftype[4];} WavHeader =
                                                       {
                                                         {
                                                           {'R','I','F','F'}
                                                           , sizeof(FmtChunk) + sizeof(RiffChunk) + length
                                                         }
                                                         , {'W','A','V','E'}
                                                       };

  /* Writes WAV header. */

  fwrite(&WavHeader, sizeof(WavHeader), 1, file);
  fwrite(&FmtChunk, sizeof(FmtChunk), 1, file);
  fwrite(&DataChunk, sizeof(DataChunk), 1, file);
}

void sample_console(value ocaml_mode)
{
  int mode = Int_val(ocaml_mode);   

  FMOD_SYSTEM          *system  = 0;
  FMOD_SOUND           *sound   = 0;
  FMOD_RESULT            result;
  FMOD_CREATESOUNDEXINFO exinfo;
  int                    key, recorddriver, numdrivers;
  unsigned int           version;
  FILE                  *file;
  unsigned int           datalength = 0, soundlength;

  /* Creates System */

  result = FMOD_System_Create(&system);
  ERRCHECK(result);

  result = FMOD_System_GetVersion(system, &version);
  ERRCHECK(result);

  /* System initialization */

  result = FMOD_System_SetOutput(system, FMOD_OUTPUTTYPE_ALSA);
  ERRCHECK(result);

  /* Record device initialization (default) */

  result = FMOD_System_GetRecordNumDrivers(system, &numdrivers);
  ERRCHECK(result);

  char name[256];
  result = FMOD_System_GetRecordDriverInfo(system, 0, name, 256, 0);
  ERRCHECK(result);

  recorddriver = 0;

  /* Setting recording mode */

  result = FMOD_System_Init(system, 32, FMOD_INIT_NORMAL, 0);
  ERRCHECK(result);

  memset(&exinfo, 0, sizeof(FMOD_CREATESOUNDEXINFO));

  exinfo.cbsize           = sizeof(FMOD_CREATESOUNDEXINFO);
  exinfo.numchannels      = 2;
  exinfo.format           = FMOD_SOUND_FORMAT_PCM16;
  exinfo.defaultfrequency = 44100;
  exinfo.length           = exinfo.defaultfrequency
    * sizeof(short)
    * exinfo.numchannels
    * 2;

  result = FMOD_System_CreateSound(system
                                   , 0
                                   , FMOD_2D | FMOD_SOFTWARE | FMOD_OPENUSER
                                   , &exinfo
                                   , &sound);
  ERRCHECK(result);

  result = FMOD_System_RecordStart(system, recorddriver, sound, TRUE);
  ERRCHECK(result);

  if (mode == 0)
    file = fopen("sounds/temp_e.wav", "wb");
  else if (mode == 1)
    file = fopen("sounds/temp_d.wav", "wb");
  else if (mode == 2)
    file = fopen("sounds/temp_w.wav", "wb");

  /* Writes wav header. Length is yet unknown, set to 0 for now. */

  WriteWavHeader(file, sound, datalength);

  result = FMOD_Sound_GetLength(sound, &soundlength, FMOD_TIMEUNIT_PCM);
  ERRCHECK(result);

  key = 1;
  
  /* main loop */
  printf("Get ready: recording starts in...\n");
  printf("3...\n");
  Sleep(1000);
  printf("2...\n");
  Sleep(1000);
  printf("1...\n");
  Sleep(1000);
  printf("==============================================================\n");
  printf("V.O.X. : Recording mode.\n");
  printf("==============================================================\n");
  do
    {
      static unsigned int lastrecordpos = 0;
      unsigned int recordpos = 0;

      FMOD_System_GetRecordPosition(system, recorddriver, &recordpos);
      ERRCHECK(result);

      if (recordpos != lastrecordpos)
        {
          void *ptr1, *ptr2;
          int blocklength;
          unsigned int len1, len2;

          blocklength = (int)recordpos - (int)lastrecordpos;
          if (blocklength < 0)
            {
              blocklength += soundlength;
            }

          /* Locks the sound. Necessary to access some raw data. */

          FMOD_Sound_Lock(sound, lastrecordpos * 4, blocklength * 4, &ptr1, &ptr2, &len1, &len2);
          /* FMOD N.B. : *4 = stereo 16bit.  1 sample = 4 bytes. */

          /* Writes to disk. */

          if (ptr1 && len1)
            datalength += fwrite(ptr1, 1, len1, file);
          if (ptr2 && len2)
            datalength += fwrite(ptr2, 1, len2, file);

          /* Unlocks the sound. Necessary for FMOD to use again. */

          FMOD_Sound_Unlock(sound, ptr1, ptr2, len1, len2);
        }

      lastrecordpos = recordpos;

      printf("Record buffer pos = %6d : Record time = %02d:%02d\r"
             , recordpos
             , datalength / exinfo.defaultfrequency / 4 / 60
             , (datalength / exinfo.defaultfrequency / 4) % 60);

      fflush(stdout);

      FMOD_System_Update(system);

      Sleep(10);

    } while ((datalength / exinfo.defaultfrequency / 4) % 60 < 3);

  fflush(stdout);

  /* Writes wav header again, with correct length. */

  WriteWavHeader(file, sound, datalength);

  /* Of course, close the file. */

  fclose(file);

  /* Kill everyone. */

  result = FMOD_Sound_Release(sound);
  ERRCHECK(result);

  result = FMOD_System_Release(system);
  ERRCHECK(result);
}
