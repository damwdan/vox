/*
* Record to disk.
* Project: V.O.X.
* Developer: TheSandmen
*/

#ifndef RECORD_H // Include guards
#define RECORD_H // To avoid double inclusion

#define _XOPEN_SOURCE 500
#define WIDTH 768
#define HEIGHT 256
#define SPECTRUM_SIZE 512
#define RATIO (HEIGHT / 255.0)

#include <caml/alloc.h>
#include <caml/mlvalues.h>
#include <caml/memory.h>

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <sys/select.h>
#include <SDL/SDL.h>
#include <SDL/SDL_getenv.h>
#include <SDL/SDL_ttf.h>

#include "../inc/fmod.h"
#include "../inc/fmod_errors.h"
#include "../inc/wincompat.h"

/* Wav format definition */

typedef struct
{
    signed char id[4];
    int size;
} RiffChunk;

typedef struct
{
    RiffChunk chunk;
    unsigned short  wFormatTag; /* format type */
    unsigned short  nChannels; /* number of channels */
    unsigned int    nSamplesPerSec; /* sample rate */
    unsigned int    nAvgBytesPerSec; /* buffer estimation */
    unsigned short  nBlockAlign; /* block size of data */
    unsigned short  wBitsPerSample; /* number of bits per sample of mono data */
} Wav;

void SaveToWav(FMOD_SOUND *sound, int mode);
void sample(SDL_Surface *screen, TTF_Font *font, SDL_Surface *text, int mode);
void ERRCHECK(FMOD_RESULT result);
void setPixel(SDL_Surface *surface, int x, int y, Uint32 pixel);
void drawSquare(SDL_Surface *screen, int x, int y, int size);
void drawSquares(SDL_Surface *screen, int x, int y, int number, int size, int offset);
int constrain(int x, int min, int max);

#define NOISE_DELAY 1000
#define NOISE_TRESH 5
float noise_band(float spectrum, float noise, int *prec);

#endif
