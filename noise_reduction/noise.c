/*
 * Noise Reduction.
 * Project: V.O.X.
 * Developer: TheSandmen
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

//#include "../inc/fmod.h"
//#include "../inc/fmod_errors.h"
//#include "../inc/wincompat.h"


// Define the struct of the header.
struct wavfile
{
    char        id[4];          // should always contain "RIFF"
    int     totallength;    // total file length minus 8
    char        wavefmt[8];     // should be "WAVEfmt "
    int     format;         // 16 for PCM format
    short     pcm;            // 1 for PCM format
    short     channels;       // channels
    int     frequency;      // sampling frequency
    int     bytes_per_second;
    short     bytes_by_capture;
    short     bits_per_sample;
    char        data[4];        // should always contain "data"
    int     bytes_in_data;
};

int main(int argc, char *argv[]) 
{
    char *filename="record.wav";
    FILE *wav = fopen(filename,"rb");
    struct wavfile header;

    if ( wav == NULL ) 
    {
        printf("%s can't be opened, check it", filename);
        exit(1);
    }

    // Read header.
    if ( fread(&header,sizeof(header),1,wav) < 1 )
    {
        printf("Header can't be read\n");
        exit(1);
    }
    if (    header.id[0] != 'R'
         || header.id[1] != 'I' 
         || header.id[2] != 'F' 
         || header.id[3] != 'F' ) { 
        printf("RIFF not here!\n"); 
        exit(1); 
    }

    printf("Everything is ok, .wav file detected\n");

    // Read data and noise reduction.
    long sum=0;
    short value=0;
    double result=0;
    int x = 0;
    while( fread(&value,sizeof(value),1,wav) && x < 50) { //TOFIX size of samples
	x ++,
	//result = exp(-pow(value,2));
	//TOFIX BLUR GAUSSIAN OR MANUAL REMOVAL
	printf("index: %d	sample: %d ---> %f\n",x,value,result);
    }
    exit(0);
}
