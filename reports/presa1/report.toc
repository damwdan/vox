\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Manipulation du son}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}FMOD}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Lecture}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Lecture du stream d'entr\IeC {\'e}e}{4}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Ecriture dans un fichier}{4}{subsection.2.4}
\contentsline {section}{\numberline {3}Chiffrement}{6}{section.3}
\contentsline {subsection}{\numberline {3.1}L'utilisation dans V.O.X.}{6}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}ROT}{7}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}XOR}{8}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Le chiffrement de V.O.X.}{9}{subsection.3.4}
\contentsline {section}{\numberline {4}R\IeC {\'e}seau de neurones artificiels}{11}{section.4}
\contentsline {subsection}{\numberline {4.1}La th\IeC {\'e}orie}{11}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Ce qu'il reste \IeC {\`a} faire}{13}{subsection.4.2}
\contentsline {section}{\numberline {5}Interface}{14}{section.5}
\contentsline {subsection}{\numberline {5.1}LablGtk}{14}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}R\IeC {\'e}alisation et fonctionnalit\IeC {\'e}s}{15}{subsection.5.2}
\contentsline {section}{\numberline {6}Site web}{21}{section.6}
\contentsline {subsection}{\numberline {6.1}L'organisation}{21}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Le style}{22}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}L'interactivit\IeC {\'e}}{23}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Ce qu'il reste \IeC {\`a} faire}{24}{subsection.6.4}
\contentsline {section}{\numberline {7}Conclusion}{25}{section.7}
