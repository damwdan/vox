\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Traitement du signal}{2}{section.2}
\contentsline {section}{\numberline {3}Chiffrement}{3}{section.3}
\contentsline {subsection}{\numberline {3.1}Variables al\IeC {\'e}atoires}{3}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Hachage}{5}{subsection.3.2}
\contentsline {section}{\numberline {4}Le fichier .vox}{7}{section.4}
\contentsline {subsection}{\numberline {4.1}La lecture et l'\IeC {\'e}criture}{7}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Structure du fichier et les \IeC {\'e}tapes du (d\IeC {\'e})chiffrement}{9}{subsection.4.2}
\contentsline {section}{\numberline {5}R\IeC {\'e}seau de neurones artificiels}{11}{section.5}
\contentsline {section}{\numberline {6}Interface}{14}{section.6}
\contentsline {subsection}{\numberline {6.1}Fen\IeC {\^e}tre principale}{14}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Fen\IeC {\^e}tre d'enregistrement}{16}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Fen\IeC {\^e}tre de lecture}{18}{subsection.6.3}
\contentsline {section}{\numberline {7}Site web}{20}{section.7}
\contentsline {section}{\numberline {8}Conclusion}{22}{section.8}
