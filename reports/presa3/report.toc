\select@language {french}
\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}Manipulation du son}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}FMOD}{2}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Lecture}{3}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Lecture du stream d'entr\IeC {\'e}e}{3}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Ecriture dans un fichier}{4}{subsection.2.4}
\contentsline {section}{\numberline {3}Traitement du signal}{6}{section.3}
\contentsline {section}{\numberline {4}Chiffrement}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}L'utilisation dans V.O.X.}{8}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}ROT}{8}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}XOR}{9}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Le chiffrement de V.O.X.}{10}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Variables al\IeC {\'e}atoires}{11}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}Hachage}{13}{subsection.4.6}
\contentsline {section}{\numberline {5}Le fichier .vox}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}La lecture et l'\IeC {\'e}criture}{15}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Structure du fichier et les \IeC {\'e}tapes du (d\IeC {\'e})chiffrement}{17}{subsection.5.2}
\contentsline {section}{\numberline {6}Compression des donn\IeC {\'e}es}{19}{section.6}
\contentsline {subsection}{\numberline {6.1}Run-Length Encoding}{20}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Lempel-Ziv-Welch}{20}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Codage gamma et delta d'Elias}{21}{subsection.6.3}
\contentsline {section}{\numberline {7}R\IeC {\'e}seau de neurones artificiels}{24}{section.7}
\contentsline {subsection}{\numberline {7.1}La th\IeC {\'e}orie}{24}{subsection.7.1}
\contentsline {subsection}{\numberline {7.2}La pratique}{26}{subsection.7.2}
\contentsline {section}{\numberline {8}Interface}{28}{section.8}
\contentsline {subsection}{\numberline {8.1}Premi\IeC {\`e}re soutenance}{28}{subsection.8.1}
\contentsline {subsection}{\numberline {8.2}Deuxi\IeC {\`e}me soutenance}{31}{subsection.8.2}
\contentsline {subsection}{\numberline {8.3}Troisi\IeC {\`e}me soutenance}{35}{subsection.8.3}
\contentsline {section}{\numberline {9}Installation et mise \IeC {\`a} jour}{39}{section.9}
\contentsline {section}{\numberline {10}Site web}{41}{section.10}
\contentsline {section}{\numberline {11}Conclusion}{45}{section.11}
