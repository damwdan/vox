\select@language {french}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}\leavevmode {\color {blue}Pr\'esentation du projet}}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}\leavevmode {\color {blue}Pr\'esentation du groupe}}{4}{section.1.2}
\contentsline {chapter}{\numberline {2}Etat de l'art}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}\leavevmode {\color {blue}Mod\`eles de Markov Cach\'es (MMC)}}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}\leavevmode {\color {blue}Dynamic time warping (DTW)}}{7}{section.2.2}
\contentsline {section}{\numberline {2.3}\leavevmode {\color {blue}R\'eseaux de neurones artificiels}}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}\leavevmode {\color {blue}Chiffrement}}{9}{section.2.4}
\contentsline {section}{\numberline {2.5}\leavevmode {\color {blue}Solutions retenues}}{9}{section.2.5}
\contentsline {chapter}{\numberline {3}D\'ecoupage du projet}{11}{chapter.3}
\contentsline {section}{\numberline {3.1}\leavevmode {\color {blue}R\'epartition des t\^aches}}{11}{section.3.1}
\contentsline {section}{\numberline {3.2}\leavevmode {\color {blue}Planning de la r\'ealisation}}{11}{section.3.2}
\contentsline {chapter}{\numberline {4}Conclusion}{13}{chapter.4}
